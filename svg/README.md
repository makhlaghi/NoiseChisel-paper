# Correcting the SVGs

The SVG format is a simple text file like HTML. So it is really easy
to modify them for any application. In the SVGs of this folder, each
tag is put on its separate line so modifying them becomes easy. Below
are the explanations on how to modify them.


## Details:

The width of the image is specified by the two width=XXX variables and
the third path (any place you see the value after the width option).

The Text positions can be specified with the x=XXX option before each
text.

To change the width of the colored regions you have to correct the
<path> tags. The `fill' specifies the color and the `d' specifies the
positioning. The width of each path can be specified by the number
after `h'. The positioning of the second colored region is done by
setting the same value after `M' and after `H' (which should be
identical to the value after `h' in the first path)
