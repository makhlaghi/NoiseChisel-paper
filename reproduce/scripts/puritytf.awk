# puritytf.awk: Separate the magnitudes of the true and false
# detections in the purity test of Akhlaghi and Ichikawa (2015), ApJS,
# 220, 1. arXiv: 1505.01664
#
# Copyright (C) 2015, Mohammad Akhlaghi <akhlaghi@gnu.org>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.

BEGIN{
    # Make sure the necessary variables are set:
    checkifset(xcol, "xcol")
    checkifset(ycol, "ycol")
    checkifset(magcol, "magcol")

    # Make sure the column numbers are integers:
    checkifint(xcol, "xcol")
    checkifint(ycol, "ycol")
    checkifint(magcol, "magcol")

    # Set the constants:
    xmin=ymin=400
    xmax=ymax=600
    center[0]=420
    center[1]=460
    center[2]=500
    center[3]=540
    center[4]=580

    # Row counter:
    inrow=0
}

$0 !~ /^#/{
    if($magcol<90){
        xs[inrow]=$xcol
        ys[inrow]=$ycol
        mags[inrow]=$magcol
        inrow++
    }
}


# For each of the places where a profile was made (`center' array), go
# over the rows and put the magnitude of the smallest distance into
# `true.txt' if the distance is less than 10 pixels appart.
END{
    truecounter=0
    for(i=0;i<5;++i){
        for(j=0;j<5;++j){
            mindist=9999999
            for(row=0;row<inrow;++row){
                dist=sqrt( (xs[row]-center[i])*(xs[row]-center[i]) \
                           + (ys[row]-center[j])*(ys[row]-center[j]) )
                if(dist<mindist) {
                    mindist=dist
                    rowofmin=row
                }
            }

            # Select the nearest detection as true only when it is in
            # the desired region. Note that particularly on the edges
            # of the true galaxy region, a false detection might be
            # close enough.
            if(xs[rowofmin]>xmin && xs[rowofmin]<xmax      \
               && ys[rowofmin]>ymin && ys[rowofmin]<ymax){
                truerows[truecounter]=rowofmin
                truecounter++
            }
        }
    }

    # Print out the true and false rows
    for(row=0;row<inrow;++row){
        out="false.txt"
        for(i=0;i<truecounter;++i)
            if(truerows[i]==row) {
                out="true.txt"
                break
            }
        print mags[row] >> out
    }
}
