#! /bin/bash
#
# sextractor.sh: Run SExtractor on the images.
#
# The variables not defined in this script are defined in the Makefile
# which calls it. If you want to run this script independently, run it
# once alone and any undefined variable will give an error. For each
# error, specify a value for that variable by running the following
# command. They are usually directories and the names are descriptive.
#
#   export variable=value
#
# Copyright (C) 2015, Mohammad Akhlaghi <akhlaghi@astr.tohoku.ac.jp>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.





# Initial settings:
set -o nounset                  # Stop if a variable is not set.
set -o errexit                  # Stop if a program returns false.
source ./scripts/texmacros.sh   # For setting numbers within the paper.




# Check the version of SExtractor, NoiseChisel does the check
# internally as guided by the configuration file.:
sex --version | awk '{
    sexversion=sprintf("%s", "'$sexversion'")
    if($3 != sexversion) {
      print("You are running SExtractor "$3" however for an exact "     \
            "reproduction, version "sexversion" is necessary. If you "  \
            "can only test with the current version, then change the "  \
            "value of the sexversion variable in the Makefile to "$3"." \
            "But note that the same results might not be reproduced.")
      exit(1)
    }
}'



# Macros:
txt=".txt"
fits=".fits"
mockpsfconv=$mock/mockpsf.conv
mockpsf="-FILTER_NAME "$mockpsfconv
seconf="-c parameters/sextractor/sextractor.conf"





# Make the output directory:
mkdir -p $sextractor





# Make the Mock PSF in SExtractor's format:
astconvertt $mock/mockkernel.fits --output=tmp.txt
echo "CONV NONORM" > $mockpsfconv
cat tmp.txt        >> $mockpsfconv
rm tmp.txt





###############
# Purity test.
#
# Why is purity the first thing SExtractor runs on?
#
# Because, when you are doing the test, you can simply put an `exit 0'
# line after this test and also after the test on NoiseChisel. Then
# simply by running `make' from the top `Reproduce' directory, you can
# test NoiseChisel and SExtractor in one command without wasting too
# much time.
#
# Why this threshold? 0.65 also appears to detect all the objects
# (irrespective of their flags). But If you try 0.625 (middle point
# between 0.6 and 0.65), you will see one of the objects will not be
# detected, so we choose 0.6 as the flux where all objects are
# definitely detected.
name=purity
puritysethresh=0.6
for ind in 1 2 3 4
do
    sex $seconf $mockpsf -DETECT_THRESH $puritysethresh \
        $mock$name$ind$fits
    mv seg.fits $sextractor$name$ind"_seg"$fits
    mv SEresults.txt $sextractor$name$ind$txt
    mv aper.fits $sextractor$name$ind"_aper"$fits
    rm back.fits
done
addtexmacro puritysethresh $puritysethresh





# Only for SEback.tex
# ===================
#
# Note that only for these three, first we cutout the central region.
csection="--section=400:600,400:600"
for name in randomsmall onelarge severallarge
do
    astimgcrop $mock$name$fits $csection
    sex $seconf $mockpsf $name"_crop.fits"
    rm aper.fits seg.fits SEresults.txt $name"_crop.fits"
    mv back.fits $sextractor$name"_back"$fits
    rm *.log
done





# SEback and SEsensitivity
# ========================
for ind in 1 2 3 4
do
    name=sensitivity$ind
    sex $seconf $mockpsf $mock$name$fits
    mv seg.fits $sextractor$name"_seg"$fits
    mv back.fits $sextractor$name"_back"$fits
    mv SEresults.txt $sextractor$name$txt
    rm aper.fits
done





# SEback.tex and SEreal.tex
# =========================
for name in 1 2 3 5 onedge
do
    if [ $name = onedge ]; then
        inputdir=$mock
        inputpsf=$mockpsf
    else
        inputdir=$real
        inputpsf="-FILTER_NAME "$realpsf
    fi
    sex $seconf $inputpsf $inputdir$name$fits
    mv SEresults.txt $sextractor$name$txt
    mv seg.fits $sextractor$name"_seg"$fits
    mv aper.fits $sextractor$name"_aper"$fits
    mv back.fits $sextractor$name$"_back"$fits
done





# SEthresh.tex
# ============
#
# `thresh' is multiplied by ten to be easily used in the filenames.
name=onelarge
for thresh in 0.1 0.5 1 2
do
    # Once for the SEthresh.tex figure:
    sex $mock$name$fits $seconf $mockpsf -DETECT_THRESH $thresh
    mv aper.fits $sextractor$name"_t"$thresh"_aper"$fits
    mv seg.fits $sextractor$name"_t"$thresh"_seg"$fits
    mv SEresults.txt $sextractor$name"_t"$thresh$txt
    rm back.fits

    # Once for the values reported in the text:
    sex $mock$name$fits $seconf $mockpsf -DETECT_MINAREA 1 \
        -DETECT_THRESH $thresh
    mv SEresults.txt $sextractor$name"_t"$thresh"_ma1"$txt
    rm back.fits aper.fits seg.fits
done

# For how the result depends on the minimum area:
for minarea in 10 25 50 75 100
do
    sex $mock$name$fits $seconf $mockpsf -DETECT_THRESH 0.1 \
	-DETECT_MINAREA=$minarea
    mv SEresults.txt $sextractor$name"_t0.1_m"$minarea$txt
    rm back.fits aper.fits seg.fits
done
