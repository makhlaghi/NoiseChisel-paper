# mockcat.awk: Make the mock galaxy catalogs.
#
# This script makes the mock galaxy catalogs that are necessary for
# the reproduction of the results in Akhlaghi and Ichikawa
# (2015). ApJS, 220, 1. arXiv: 1505.01664.
#
# Copyright (C) 2015, Mohammad Akhlaghi <akhlaghi@astr.tohoku.ac.jp>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.
#
# Library functions:
# ==================
#
# This program needs the following outside function(s). The file
# containing these functions must be called along with this source
# file by AWK from the command line (with a -i option).
#
# checkifset()

# Check if the output directory is set:
BEGIN{
    checkifset(outdir, "senq")
    checkifset(outdir, "senre")
    checkifset(outdir, "outdir")
    checkifset(outdir, "truncr")
    checkifset(outdir, "purityn")
    checkifset(outdir, "sentheta")
    checkifset(outdir, "puritymag")
    checkifset(outdir, "senmagdiff")
    checkifset(outdir, "senfaintmag")
}




# Write all the parameter files:
END{
    # Fixed string for creating TeX macros:
    fixed="source scripts/texmacros.sh && addtexmacro"
    command=sprintf("%s moffatbeta %s", fixed, moffatbeta); system(command)


    # Kernels for the convolution demonstration, changing these will
    # only change the convolution demo figure..
    for(i=0;i<5;++i)
    {
        print(0, 0, 0, 1, demofwhms[i], moffatbeta, 0, 1, 1,
              truncr) > outdir"/demokernels.txt"

        if(i==0) cc="a"; else if (i==1) cc="b"; else if (i==2) cc="c";
        else if(i==3) cc="d"; else if (i==4) cc="e"; else exit(1);
        command=sprintf("%s convdemo%s %s", fixed, cc, demofwhms[i]);
        system(command)
    }

    # Kernels used by other programs. Changing these will change the
    # outcome of those programs.
    print(0, 0, 0, 1, mockfwhm, moffatbeta, 0, 1, 1,
          truncr) > outdir"/mockkernel.txt"
    command=sprintf("%s mockfwhm %s", fixed, mockfwhm); system(command)


    # Sensitivity tests:
    xs[0]=420; xs[1]=460; xs[2]=500; xs[3]=540; xs[4]=580;
    ys[0]=420; ys[1]=460; ys[2]=500; ys[3]=540; ys[4]=580;
    for(i=0;i<4;++i){
        outfile=sprintf("%s/sensitivity%d.txt", outdir, i+1)
        mag=senfaintmag
        for(j=0;j<5;++j)
            for(k=0;k<5;++k){
                print(0, xs[k], ys[j], 0, senre, ns[i], sentheta,
                      senq, mag, truncr) > outfile
                mag+=senmagdiff
            }

        if(i==0) cc="a"; else if (i==1) cc="b"; else if (i==2) cc="c";
        else if(i==3) cc="d"; else exit(1);
        command=sprintf("%s senn%s %s", fixed, cc, ns[i]); system(command)
    }


    # Purity:
    for(j=0;j<5;++j)
        for(k=0;k<5;++k)
            print(0, xs[k], ys[j], 0, senre, purityn, sentheta,
                  senq, puritymag, truncr) > outdir"/purity.txt"



    # Save all the parameters:
    command=sprintf("%s senq %s", fixed, senq); system(command)
    command=sprintf("%s senre %s", fixed, senre); system(command)
    command=sprintf("%s truncr %s", fixed, truncr); system(command)
    command=sprintf("%s purityn %s", fixed, purityn); system(command)
    command=sprintf("%s sentheta %s", fixed, sentheta); system(command)
    command=sprintf("%s puritymag %s", fixed, puritymag); system(command)
    command=sprintf("%s senmagdiff %s", fixed, senmagdiff); system(command)
    command=sprintf("%s senfaintmag %s", fixed, senfaintmag); system(command)
    command=sprintf("%s senbrightmag %.2f", fixed,
                    senfaintmag+(24*senmagdiff)); system(command)
}
