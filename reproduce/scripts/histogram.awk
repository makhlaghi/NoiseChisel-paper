# histogram.awk: Make a histogram of the input data.
#
# Copyright (C) 2015, Mohammad Akhlaghi <akhlaghi@gnu.org>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.
#
#
#
#
# This script goes over the given input column and counts how many of
# the data elements (rows) are within the given range.
#
# Library functions:
# ==================
#
# This program needs two outside functions:
#
# checkifset()
# checkifint()
#
#
# Variables:
# ==========
#
# out:            Output file name.
# min:            Minimum value for the histogram.
# max:            Maximum value for the histogram.
# numbins:        Number of bins in the histogram.
# col:            The column that should be used.
# showemptylast:  If ==1, then show the last bins if
#                    they are empty (with a zero value).
#
# Example:
# ========
#
# Lets assume you have put all the variables into the shell variable
# AWKVAR. So you can run this script with:
#
# awk $AWKVAR -i /path/to/checks.awk -f /path/to/histogram.awk input.txt





# Do the preparations:
BEGIN {
    # Check if all the variables are set:
    checkifset(out, "out")
    checkifset(col, "col")
    checkifset(max, "max")
    checkifset(min, "min")
    checkifset(numbins, "numbins")
    checkifset(showemptylast, "showemptylast")

    # Check if col is an integer:
    checkifint(col, "col")
    checkifint(showemptylast, "showemptylast")

    # Set the required variables (macros)
    width=(max-min)/numbins
}




# Go over each line (record) in the input file. If it is commented
# (starts with a #) then ignore it. This histogram finding method uses
# the special feature of AWK arrays: that the indexs have names, they
# are not ordered, they can be added any time and they initiate with
# zero. For each field checked, the appropriate counter is incremented
# by one.
$0 !~ /^#/ {

    # If the value is smaller or larger than the desired region, then
    # go to the next record. This is not of interest.
    if($col < min || $col > max) next

    # Find the proper middle bin point for this field (record).
    midbin = min + int( ($col-min)/width )*width + width/2

    # If $col==max, then midbin will be larger than max. In that case,
    # we want it to be added to the last bin.
    if(midbin>max) {
        if($col==max) midbin-=width
        else next
    }

    # Convert the midbin value to a string, so floating point errors
    # don't bother with the counting.
    strmidbin=sprintf("%.3f", midbin)

    # Increment the value of this bin by one.
    histogram[strmidbin]++;
}





# Print out the results.
END {
    for(i=0; i<numbins; ++i){
        strmidbin=sprintf("%.3f", min+i*width+width/2)
        if(showemptylast==0 && histogram[strmidbin]==0) continue
        printf("%-10.3f %-5d\n", min+i*width+width/2,
               histogram[strmidbin]) > out
    }
}
