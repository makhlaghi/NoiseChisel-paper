#! /bin/bash
#
# real.sh: Prepare the real images.
#
# The variables not defined in this script are defined in the Makefile
# which calls it. If you want to run this script independently, run it
# once alone and any undefined variable will give an error. For each
# error, specify a value for that variable by running the following
# command. They are usually directories and the names are descriptive.
#
#   export variable=value
#
# Copyright (C) 2015, Mohammad Akhlaghi <akhlaghi@astr.tohoku.ac.jp>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.




# Initial settings:
set -o nounset                  # Stop if a variable is not set.
set -o errexit                  # Stop if a program returns false.





# Macros:
txt=".txt"
fits=".fits"
tmppos="positions.txt"




# Make the output directory:
mkdir -p $real




# Check if the cutouts have already been prepared:
numexisting=0
for name in 1 2 3 4 5 6
do
    if [ -f $CUTOUTS/$name$fits ]; then ((numexisting+=1)); fi
done





# Put the cutouts in the appropriate place:
if [ -d $CUTOUTS ] && [ $numexisting = 6 ]; then

    # Note that we need to copy the files in the real directory. If a
    # symbolic link is used to the CUTOUTS directory (to avoid
    # copying), then the time stamps of the files will not change and
    # so, this script will always execute.
    for name in 1 2 3 4 5 6
    do
        cp $CUTOUTS/$name$fits $real
    done

else

    if [ -L $real ]; then rm $real; fi

    # Remove all the comments from the IRAC formatted file:
    awk '$0 !~ /^\\/ && $0 !~ /^\|/ {print $0}' $realpositions > $tmppos

    # Cutout all the desired regions using ImageCrop:
    astimgcrop $tmppos $COSMOSACS/*_sci.fits --racol=1 --deccol=2 \
               --wwidth=30 --suffix=.fits --output=$real

    # Clean up:
    rm astimgcrop.log $tmppos
fi
