#! /bin/bash
#
# mock.sh: Make the mock input images
#
# The variables not defined in this script are defined in the Makefile
# which calls it. If you want to run this script independently, run it
# once alone and any undefined variable will give an error. For each
# error, specify a value for that variable by running the following
# command. They are usually directories and the names are descriptive.
#
#   export variable=value
#
# Copyright (C) 2015, Mohammad Akhlaghi <akhlaghi@astr.tohoku.ac.jp>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.




# Initial settings:
set -o nounset                  # Stop if a variable is not set.
set -o errexit                  # Stop if a program returns false.
source ./scripts/texmacros.sh   # For setting numbers within the paper.




# Macros:
txt=".txt"
fits=".fits"
export GSL_RNG_SEED=1
oversample="--oversample=3"
export GSL_RNG_TYPE=ranlxs2
mockpsf=$mock/mockkernel.fits
fixedmockparams=$parameters/mock/
ovmockpsf=$mock/mockkernel_oversampled.fits
warpmatrix="--matrix=0.33333,0,0.33333,0,0.33333,0.33333,0,0,1"





# Make the output directory if it isn't already made:
mkdir -p $mock









################################################################
################         Functions         #####################
################################################################
# Repeated actions:
# 1. Delete all outputs.
# 2. Make the profile on an oversampled grid.
# 3. Convolve it with the PSF.
# 4. Warp it to the correct pixel grid (resample it).
# 5. Add noise to it.
# 6. Move the outputs to the desired directory.
# 7. Remove extra files.
# 8. Increment the random number generator seed.
completemock ()
{
    # Set the base name:
    tmp=${1%.txt}
    base=${tmp##*/}

    # Do the preparations:
    export GSL_RNG_SEED=$2
    rm -f $mock$base"_nn"$fits $mock$base$fits

    # Do the job:
    astmkprof --envseed $1 $oversample
    astconvolve --kernel=$ovmockpsf $base$fits
    astimgwarp $warpmatrix $base"_convolved"$fits
    astmknoise --envseed $base"_convolved_warped"$fits

    # Clean up:
    mv $base"_convolved_warped.fits" $mock$base"_nn"$fits
    mv $base"_convolved_warped_noised.fits" $mock$base$fits
    rm *.fits *.log
}




# When the image is only meant to be 200x200 pixels. The PSF is 17x17
# and since 17=2*8+1, then we make the mock image with
# (200+2*8)x(200+2*8) pixels and then crop the edge 8 pixels in the
# end.
completemock_216_nooversample()
{
    # Set the base name:
    tmp=${1%.txt}
    base=${tmp##*/}

    # Do the preparations:
    export GSL_RNG_SEED=$2
    rm -f $mock$base"_nn"$fits $mock$base$fits

    # Do the job:
    astmkprof --envseed $1 --naxis1=216 --naxis2=216 --oversample=1
    astconvolve --kernel=$mockpsf $base$fits
    astimgcrop --section=8:*-8,8:*-8 $base"_convolved"$fits \
               --zeroisnotblank
    astmknoise --envseed $base"_convolved_crop"$fits

    # Clean up:
    mv $base"_convolved_crop_noised"$fits $mock$base$fits
    rm *.fits *.log
}










################################################################
################        Main script        #####################
################################################################

# First, make the kernels used in the convolution demonstration:
astmkprof --envseed $oversample $mockparams/demokernels.txt
astimgwarp $warpmatrix 0.fits --output=$mock/convdemo1$fits
astimgwarp $warpmatrix 1.fits --output=$mock/convdemo2$fits
astimgwarp $warpmatrix 2.fits --output=$mock/convdemo3$fits
astimgwarp $warpmatrix 3.fits --output=$mock/convdemo4$fits
astimgwarp $warpmatrix 4.fits --output=$mock/convdemo5$fits
rm 0.fits 1.fits 2.fits 3.fits 4.fits demokernels.fits astmkprof.log

# Make the kernel used for mock imags:
astmkprof --envseed $oversample $mockparams/mockkernel.txt
astimgwarp $warpmatrix 0.fits --output=$mock/mockkernel$fits
mv 0.fits $ovmockpsf
rm astmkprof.log mockkernel.fits





# Mock images for the respective figures:

# dataandnoise.tex
completemock $fixedmockparams/onelarge.txt                1
completemock $fixedmockparams/randomsmall.txt             2
completemock $fixedmockparams/severallarge.txt            3

# Tests:
completemock $mockparams/sensitivity1.txt                 4
completemock $mockparams/sensitivity2.txt                 5
completemock $mockparams/sensitivity3.txt                 6
completemock $mockparams/sensitivity4.txt                 7
completemock $fixedmockparams/onedge.txt                  8

#mode.tex:
completemock_216_nooversample $fixedmockparams/mode1.txt  9
completemock_216_nooversample $fixedmockparams/mode2.txt 10

# Purity:
completemock $mockparams/purity.txt                      20
mv $mock/"purity"$fits $mock/"purity1"$fits
export GSL_RNG_SEED=21
astmknoise --envseed $mock/"purity_nn"$fits
mv purity_nn_noised.fits $mock/"purity2"$fits
export GSL_RNG_SEED=22
astmknoise --envseed $mock/"purity_nn"$fits
mv purity_nn_noised.fits $mock/"purity3"$fits
export GSL_RNG_SEED=23
astmknoise --envseed $mock/"purity_nn"$fits
mv purity_nn_noised.fits $mock/"purity4"$fits




# Profiles for getting the total flux of the onelarge profile:

# Set the random number seed.
export GSL_RNG_SEED=1

# If you change the profile parameters of the onelarge profile,
# then uncomment the next two lines to make it and use the
# magnitude of the top pixel in it in the `onelarge_90.txt' and
# `onelarge_95' profiles.
#astmkprof --envseed $MOCKPRMDIR"onelarge"$TXT --oversample=1
#exit 0

# Make the three profiles:
for name in onelarge_fullflux onelarge_95 onelarge_90
do
    astmkprof --envseed --magatpeak $fixedmockparams$name$txt  \
              --oversample=1 --naxis1=2000 --naxis2=2000
    astimgstat $name$fits --nohist --nocfp --nosigclip \
               --noasciihist > $mock/$name$txt
    mv $name$fits $mock
done

# Clean up:
rm *.log
