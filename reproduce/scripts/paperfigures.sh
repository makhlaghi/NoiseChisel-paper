#! /bin/bash
#
# paperfigures.sh: Prepare all the inputs for LaTeX/PGFPlots.
#
# The variables not defined in this script are defined in the Makefile
# which calls it. If you want to run this script independently, run it
# once alone and any undefined variable will give an error. For each
# error, specify a value for that variable by running the following
# command. They are usually directories and the names are descriptive.
#
#   export variable=value
#
# Copyright (C) 2015, Mohammad Akhlaghi <akhlaghi@astr.tohoku.ac.jp>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.





# Initial settings:
set -o nounset                  # Stop if a variable is not set.
set -o errexit                  # Stop if a program returns false.
source ./scripts/texmacros.sh   # For setting numbers within the paper.




# Macros:
eps=".eps"
jpg=".jpg"
seg="_seg"
txt=".txt"
det="_det"
cfp="_cfp"
hist="_hist"
crop="_crop"
aper="_aper"
fits=".fits"
stats="_stats"
warp="_warped"
lab="_labeled"
mdet="_maskdet"
back="_back"
conv="_convolved"
crophist="_crop_hist"
nhc="--nohist --nocfp"
csection="--section=401:601,401:601"
correctfill="!/^#/{print(\$1,\$2,\$3,4,\$9,0,\$8,\$10/\$9,0,\$6)}"
correctcircum="!/^#/{print(\$1,\$2,\$3,5,\$9,0,\$8,\$10/\$9,0,\$6)}"










# Write the TeX macros:
addtexmacro figtexdir $texdir
addtexmacro scccdname $scccdname






# Function:
function findhistlowhigh
{
    lowhigh=$(awk '!/^#/{
                     counter++
                     if(counter==1) first=$1
                     else if(counter==2) binwidth=$1-first
                     else next
                   }
                   END{printf("%.6f %.6f\n",
                              first-binwidth/2, $1+binwidth/2)}' $1 )
    low=$(echo $lowhigh | awk '{print $1}')
    high=$(echo $lowhigh | awk '{print $2}')
}












# Basic noise information
# =======================
#
# These are needed for the thresholds of the images, so they are set
# here instead of papernumbers.sh
mknoiseconf=$parameters/gnuastro/astmknoise.conf
zeropoint=$(awk '!/^#/ && $1 == "zeropoint"{print $2; exit 0}' \
                $mknoiseconf)
backmag=$(awk '!/^#/ && $1 == "background"{print $2; exit 0}'  \
              $mknoiseconf)
backcount=$(echo " " | awk '{
    print 10^( ('$backmag'-'$zeropoint')/-2.5 )}')
backstd=$(echo " " | awk '{print sqrt('$backcount')}')
backthresh=$(echo " " | awk '{print '$backcount'+'$backstd'}')
addtexmacro backmag $backmag
addtexmacro backstd $backstd
addtexmacro backcount $backcount
addtexmacro zeropoint $zeropoint
addtexmacro backthresh $backthresh







###################################################
###################################################
# If you want to skip a large portion of the scripts, then uncomment
# the line below and put a `fi' wherever you want to start executing.
#if false; then
###################################################
###################################################


# dataandnoise.tex
# ================
#
# Figure 1: The no noise statistics do not ask for sigma-clipping
# because it will not converge.
dnlow=-250
dnhigh=1000
cdnlow=$((backcount+dnlow))
cdnhigh=$((backcount+dnhigh))
outdir=$texdir"/dataandnoise/"
fluxrange="--fluxlow="$dnlow" --fluxhigh="$dnhigh
nfluxrange="--fluxlow="$cdnlow" --fluxhigh="$cdnhigh
cdnmhigh=$(echo " " | awk '{print '$backcount'+('$dnhigh'/2)}')
cdnNhigh=$(echo " " | awk '{print '$backcount'+('$dnhigh'/4)}')
histrange="--histmin="$dnlow" --histmax="$dnhigh" --histnumbins=400"
nhistrange="--histmin="$cdnlow" --histmax="$cdnhigh" --histnumbins=400"
mkdir -p $outdir
for name in randomsmall onelarge severallarge
do
    # First do the no noise image:
    if [ $name = severallarge ]; then
        scale=" "
    else
        scale="--log"
    fi
    nnname=$name"_nn"           # For no-noise-name
    astimgcrop $csection $mock$nnname$fits --zeroisnotblank
    astconvertt $scale $nnname$crop$fits $fluxrange \
                --output=$outdir$nnname$eps
    astimgstat -q $histrange --nocfp --nosigclip $nnname$crop$fits
    mv $nnname$crophist$txt $outdir

    # Then the noised image:
    astimgcrop $csection $mock$name$fits
    astconvertt $name$crop$fits $nfluxrange \
                --output=$outdir$name$eps
    astimgstat $nhistrange --nocfp $name$crop$fits  \
               > $outdir$name$stats$txt
    mv $name$crophist$txt $outdir

    # Get the number of pixels above the sky value:
    if [ $name = onelarge ]; then
        astconvertt $nnname$crop$fits --output=tmp.txt
        onelargefracabove=$(awk '!/^#/{for(i=0;i<NF;++i){
               if($i>0)c++; if($i>'$backstd') h++}}
             END{printf("%.2f", 100*h/c)}' tmp.txt)
        addtexmacro onelargefracabove $onelargefracabove
        rm tmp.txt
    fi

    # Get the multiple of sigma of the brightest pixel:
    if [ $name = severallarge ]; then
        astconvertt $nnname$crop$fits --output=tmp.txt
        maxpix=$(awk 'BEGIN{max=-9999}
                      !/^#/{for(i=0;i<NF;++i) if($i>max) max=$i}
                      END{printf("%.2f", max/'$backstd')}' tmp.txt)
        addtexmacro severallargemaxpix $maxpix
        rm tmp.txt
    fi

    # Clean up:
    rm $name$crop$fits $nnname$crop$fits astimgcrop.log
done
addtexmacro datanoiselow $dnlow
addtexmacro datanoisehigh $dnhigh





# det.tex
# =======
#
# Figure 3.
outdir=$texdir"/det/"
mkdir -p $outdir
for name in onelarge sensitivity2 sensitivity3 4
do
    # Make the no noised images for the mock cases and set the
    # constants.
    if [ $name != 4 ]; then
        fluxrange="--fluxlow=$dnlow --fluxhigh=$dnhigh"
        nfluxrange="--fluxlow=$cdnlow --fluxhigh=$cdnhigh"
        astimgcrop $mock$name"_nn"$fits $csection --zeroisnotblank
        astconvertt --log $name"_nn"$crop$fits $fluxrange \
                    --output=$outdir$name"_nn"$eps
        rm $name"_nn"$crop$fits
    else
        nfluxrange="--fluxlow=-0.01 --fluxhigh=0.02"
    fi


    # Save the object and background masks. Note that this has to be
    # before the steps, because we need the nfluxrange value.
    for hdu in 1 2
    do
        # Crop and convert:
        astimgcrop --keepblankcenter --hdu=$hdu $csection    \
                   $noisechisel$name$mdet$fits
        astconvertt --output=$outdir$name"_m"$hdu$eps        \
                    $nfluxrange $name$mdet$crop$fits

        # Get the statistics of the undetected regions:
        if [ $hdu = 2 ]; then
            astimgstat $nhc $name$mdet$crop$fits > $outdir$name$stats$txt
        fi

        # Clean up:
        rm $name$mdet$crop$fits
    done


    # Go over the detection steps.
    for hdu in 0 1 2 3 4
    do
        if [ $hdu = 2 ] || [ $hdu = 3 ] || [ $hdu = 4 ] ; then
            nfluxrange=" "
        fi
        astimgcrop $noisechisel$name$det$fits --hdu=$hdu $csection
        astconvertt --output=$outdir$name"_"$hdu$eps   \
                    $nfluxrange $name$det$crop$fits
        rm $name$det$crop$fits
    done

    # Save catalog of onelarge for the analysis table:
    if [ $name = onelarge ]; then
        cp $noisechisel$name"_labeled_o"$txt $outdir
    fi


    # Clean up:
    rm astimgcrop.log
done










# convolution.tex:
# ================
#
# Figure 4: The biggest FWHM is 103=2*51+1 pixels on each side. So to
# speed things up, we will crop the region 350 to 650 instead of the
# initial 400 to 600 region. Do the convolution, then cutout the 50
# pixels on the edges. In this manner, the edge effects of none of the
# PSFs will affect the desired region.
name=sensitivity2

counter=1
input=$name$crop
outdir=$texdir"/convolution/"
astimgcrop $mock$name$fits --section=350:650,350:650
astimgcrop $mock$name$fits $csection --output=base.fits
mkdir -p $outdir
for ind in 1 2 3 4 5
do
    # Convolve the image, crop it and make the eps:
    psfname=convdemo$ind
    astconvolve --kernel=$mock$psfname$fits $input$fits
    astimgcrop $input$conv$fits --section=50:*-50,50:*-50
    astconvertt $input$conv$crop$fits --borderwidth=5                 \
                --output=$outdir$name$psfname$eps
    rm $input$conv$fits


    # Get the convolved histogram.
    astimgstat --nocfp --histnumbins=100 --histquant=0.001            \
               $input$conv$crop$fits > $outdir$psfname$stats$txt
    mv $input$conv$crop$hist$txt $outdir$psfname$hist$txt


    # Count the number of pixels above 1sigma on the second kernel:
    if [ $ind = 2 ]; then
        astconvertt $input$conv$crop$fits --output=tmp.txt
        numhigher=$(awk '!/^#/{for(i=1;i<=NF;++i)
                           if($i>('$backcount'+'$backstd')) counter++}
                         END{print counter}' tmp.txt)
        addtexmacro convsampbnumhigher $numhigher
        rm tmp.txt
    fi
    rm $input$conv$crop$fits


    # Find the flux range of the convolved histogram:
    findhistlowhigh $outdir$psfname$hist$txt


    # Get the input histogram within the range of the convolved image.
    astimgstat -q base.fits --nocfp --histnumbins=100                \
               --histmin=$low --histmax=$high
    mv base$hist$txt $outdir$psfname"b"$hist$txt

    # Clean up:
    rm astimgcrop.log
done
rm base.fits $name$crop$fits







# NCthresh.tex
# ============
#
# Figure 5.
outdir=$texdir"/NCthresh/"
mkdir -p $outdir
for name in onelarge sensitivity2 sensitivity3 4
do
    # Set the quantiles for the input images:
    if [ $name = onelarge ] || [ $name = 4 ]; then
	histquant="--histquant=0.05"
    else
	histquant="--histquant=0.001"
    fi

    # First get the convolved image histograms and statistics.
    astimgcrop $noisechisel$name$det$fits --hdu=1 $csection
    astimgstat $name$det$crop$fits --histnumbins=100 $histquant \
               --cfpsimhist --maxcfpeqmaxhist \
               > $outdir$name"_c"$stats$txt
    mv $name$det$crop$hist$txt $outdir$name"_chist"$txt
    mv $name$det$crop$cfp$txt $outdir$name"_ccfp"$txt
    rm $name$det$crop$fits

    # Find the histogram low and high values:
    findhistlowhigh $outdir$name"_chist"$txt

    # Now get the image statistics for the unconvolved image
    astimgcrop $noisechisel$name$det$fits --hdu=0 $csection
    astimgstat $name$det$crop$fits --histnumbins=100 \
               --histmin=$low --histmax=$high --nocfp
    mv $name$det$crop$hist$txt $outdir$name$hist$txt
    rm $name$det$crop$fits

    # Clean up:
    rm astimgcrop.log
done









# dettf.tex
# =========
#
# Figure 7.
outdir=$texdir"/dettf/"
mkdir -p $outdir
for name in onelarge sensitivity3 4
do
    # Create the proper eps images.
    for hdu in 4 7 8 10 11 12 14 15
    do
        astimgcrop $noisechisel$name$det$fits --hdu=$hdu $csection
        astconvertt $name$det$crop$fits \
                    --output=$outdir$name"_"$hdu$eps
        rm $name$det$crop$fits
    done

    # Copy the histograms to the output directory:
    if [ $name = onelarge ]; then
        suffix="_7_detsn.txt"
    else
        suffix="_12_detsn.txt"
    fi
    cp $noisechisel$name$suffix $outdir

    # Clean up:
    rm astimgcrop.log
done










# overseg.tex
# ===========
#
# Figure 8.
name=overseg$fits
#You have to set the colors by hand (don't forget that the order the
#conversion is done is from right to left):
outdir=$texdir"/overseg/"
mkdir -p $outdir
for i in {0..4}
do
    for j in {0..24}
    do
	if [ $i == 0 ] && [ $j == 0 ]; then
	    ll=" "
	else
	    ll="--fluxlow=0 --fluxhigh=7 --fhmaxbyte "
            ll=$ll"--change=-2:0,2:1,7:2,6:3,10:4,16:5,-1:7"
	fi
        astconvertt $images$name $ll --hdu=$(($i*25+$j)) \
                    --output=$outdir$i\_$j.eps --widthincm=20
        echo $outdir$i\_$j.eps created
    done
done









# segtf.tex
# =========
#
# Figure 9.
outdir=$texdir"/segtf/"
mkdir -p $outdir
realfluxrange="--fluxlow=-0.005 --fluxhigh=0.02"
for name in onelarge 1 3 5
do
    for hdu in 0 1 3 4 5
    do
        # Note that in the segmentation check image, the first
        # extension is the input image (not sky subtracted, while in
        # the second (the convolved image) is sky subtracted.
        if [ $hdu = 0 ] || [ $hdu = 1 ]; then
            if [ $name = onelarge ]; then
                if [ $hdu = 0 ]; then
                    options="--fluxlow=$cdnlow --fluxhigh=$cdnhigh"
                else
                    options="--fluxlow=$dnlow --fluxhigh=$dnhigh"
                fi
            else
                options=$realfluxrange
            fi
        else
            options="--fluxlow=-2 --fluxhigh=1 --changeaftertrunc "
            options=$options" --change=-2:1001,-1:1040,0:1000,1:1005"
        fi
        astimgcrop $noisechisel$name$seg$fits --hdu=$hdu $csection
        astconvertt $options $name$seg$crop$fits \
                    --output=$outdir$name"_"$hdu$eps
        rm $name$seg$crop$fits

        # Clean up:
        rm astimgcrop.log
    done
    cp $noisechisel$name"_12_segsn.txt" $outdir
done










# objseg.tex
# ==========
#
# Figure 10. In the last row, the pixel labels of the objects covering
# the central detection are changed to very large values then through
# a flux threshold the rest of the image pixels are made white.
#
# So if other NoiseChisel parameters are used or other input
# images are used, please inspect the labels of the different
# objects you want to highlight and put them here (before the `:'
# sign).
#
# The labels can be seen in the 11th extension of the _seg.fits
# images in TMPDIR/noisechisel. The image names used can be seen
# above this comment in the `for name in ...' line.
outdir=$texdir"/objseg/"
mkdir -p $outdir
realfluxrange="--fluxlow=-0.005 --fluxhigh=0.02"
for name in 1 3 6
do
    # Set the changing values here to make the next loop clean.
    if [ $name = 1 ]; then
        changeops="--change=109:100001,110:100002,111:100003,112:100004"
    elif [ $name = 3 ]; then
        changeops="--change=95:100001,96:100002"
    else
        changeops="--change=203:100001,204:100002,205:100003"
    fi

    # Loop over the desired hdus and convert them to eps.
    for hdu in 0 5 6 7 10
    do
        if [ $hdu = 0 ]; then
            options=$realfluxrange
        elif [ $hdu = 10 ]; then
            options=$changeops" --fluxlow=1e5 --fluxhigh=2e5"
        else
            options="--change=0:-4"
        fi

        astimgcrop $noisechisel$name$seg$fits --hdu=$hdu $csection
        astconvertt $options $name$seg$crop$fits \
                    --output=$outdir$name"_"$hdu$eps
        rm $name$seg$crop$fits

        # Clean up:
        rm astimgcrop.log
    done
done










# largeimage.tex
# ==============
#
# Figure 11. Warp the image to make the number of pixels smaller by 8
# times, this is necessary for having a reasonable file size! Since
# the image is has four independent amplifiers which are 256 pixels
# wide, it is best that it be divisible by the fraction to shrink the
# image.
#
# The reason sky subtracted image looks very different from the
# published version is the fact that in the published version I used
# ImageMagick to rescale the image. But now I am using
# ImageWarp. ImageMagick uses interpolation to re-sample the image,
# but in ImageWarp, the fraction of flux within the pixel area is
# used. So the low surface brightness regions are high-lighted in this
# methodology and missed (smoothed out) in the interpolation methods
# since they consider the pixels to be points devoid of any area.
calledonce=0
name=$scccdname
outdir=$texdir"/largeimage/"
regioncrop="--section=1236:1934,550:817"
ssfluxrange="--fluxlow=-1000 --fluxhigh=1700"
innerfluxrange="--fluxlow=4050 --fluxhigh=4150"
influxrange="--fluxlow=257000 --fluxhigh=267000"
warpmatrix="--matrix=0.125,0,0.4375,0,0.125,0.4375,0,0,1"
mkdir -p $outdir
for suffix in _maskdet _maskdet _skysubed
do
    # Set the main hdus and flux ranges
    if [ $suffix = _maskdet ]; then
        if [ $calledonce = 0 ]; then
            hdu=0
            calledonce=1
            outsuffix="_input"
            fluxrange=$influxrange
        else
            hdu=2
            outsuffix=$mdet
            fluxrange=$influxrange
        fi
    elif [ $suffix = _skysubed ]; then
        hdu=0
        outsuffix="_skysubed"
        fluxrange=$ssfluxrange
    fi

    astimgwarp $noisechisel$name$suffix$fits --maxblankfrac=0.2  \
               --hdu=$hdu $warpmatrix
    astconvertt $name$suffix$warp$fits --widthincm=40 $fluxrange \
                --output=$outdir$name$outsuffix$eps
    echo $outdir$name$suffix$eps ready $fluxrange
    rm $name$suffix$warp$fits

    # Make the two bottom crops of this image.
    if [ $suffix = _maskdet ]; then
        for hdu in 0 2
        do
            astimgcrop $regioncrop --hdu=$hdu                    \
                       $noisechisel$name$suffix$fits
            astconvertt $name$suffix$crop$fits $innerfluxrange  \
                        --output=$outdir$name"_"$hdu$eps --widthincm=20
            rm $name$suffix$crop$fits astimgcrop.log
        done
    fi
done
astimgwarp $noisechisel$name$lab$fits --maxblankfrac=0.2 --hdu=3 \
           $warpmatrix
astconvertt $name$lab$warp$fits --output=$outdir$name"_sky"$eps  \
            $influxrange
rm $name$lab$warp$fits









# NCsensitivity.tex
# =================
#
# Figure 12.
outdir=$texdir"/NCsensitivity/"
fluxrange="--fluxlow=$cdnlow --fluxhigh=$cdnhigh"
mkdir -p $outdir
for ind in 1 2 3 4
do
    name=sensitivity$ind
    for hdu in 0 1 2
    do
        # Make the eps image:
        astimgcrop $noisechisel$name$mdet$fits --hdu=$hdu $csection \
                   --keepblankcenter
        astconvertt $name$mdet$crop$fits $fluxrange \
                    --output=$outdir$name"_"$hdu$eps

        # Get the statistics of the undetected regions:
        if [ $hdu = 2 ]; then
            astimgstat $name$mdet$crop$fits \
                       > $outdir$name$stats$txt
        fi

        # Put the total number of detections in the TeX directory:
        cp $noisechisel$name"_labeled_o".txt $outdir
    done
done









# NCreal.tex
# ==========
#
# Figure 13.
outdir=$texdir"/NCreal/"
realfluxrange="--fluxlow=-0.01 --fluxhigh=0.015"
mkdir -p $outdir
for name in 2 1 3 5 onedge
do
    if [ $name = onedge ]; then
        TOCROP="--section=1:201,1:201"
        fluxrange="--fluxlow=$cdnlow --fluxhigh=$cdnmhigh"

        # Necessary for finding the number of false detections:
        cp $noisechisel$name"_labeled_o"$txt $outdir
    else
        TOCROP=$csection
        fluxrange=$realfluxrange
    fi
    for hdu in 0 1 2
    do
        # Crop out the detected regions:
        astimgcrop $noisechisel$name$mdet$fits --hdu=$hdu $TOCROP \
                   --keepblankcenter
        astconvertt $name$mdet$crop$fits $fluxrange \
                    --output=$outdir$name"_"$hdu$eps

        # Get the statistics of the undetected regions:
        if [ $hdu = 2 ]; then
            astimgstat $name$mdet$crop$fits \
                       > $outdir$name$stats$txt
        fi
    done
done
rm -f *.fits *.txt









# purity.tex
# ==========
#
# Figure 14.
xawkcol=2
yawkcol=3
numbins=50
worstind=3
histmax=-5.6
histmin=-11.8
name=purity
shrink=0.333
outdir=$texdir"/purity/"
histopt="-i $scripts/checks.awk -v min=$histmin -v max=$histmax"
histopt=$histopt" -v numbins=$numbins -v col=1 -v showemptylast=1"
histopt=$histopt" -f $scripts/histogram.awk"
warpmatrix="--matrix=$shrink,0,$shrink,0,$shrink,$shrink,0,0,1"

mkdir -p $outdir

# First draw SExtractor's results:
awk '!/^#/{print($1, $2/3, $3/3, 4, $9, 0, $8, $10/$9, 0, $6/3)}'   \
    $sextractor$name$worstind$txt > tmp.txt
astimgwarp $mock$name$worstind$fits $warpmatrix
astimgcrop $name$worstind$warp$fits --section="2:,2:"
astmkprof tmp.txt $name$worstind$warp$crop$fits --oversample=1           \
          --setconsttomin --replace
astconvertt --widthincm=40 tmp.fits --output=$outdir$name"_se"$eps
rm $name$worstind$warp$crop$fits $name$worstind$warp$fits
rm astimgcrop.log astmkprof.log tmp.fits tmp.txt

# Then make SExtractor's images
astimgwarp $noisechisel$name$worstind$mdet$fits --hdu=2 $warpmatrix      \
           --maxblankfrac=0.5
astimgcrop $name$worstind$mdet$warp$fits --section="2:,2:"               \
           --keepblankcenter --output=tmp.fits
astconvertt --widthincm=40 tmp.fits --output=$outdir$name"_nc"$eps
rm astimgcrop.log tmp.fits $name$worstind$mdet$warp$fits


# Make the separate histograms:
for indir in $noisechisel $sextractor
do
    # To find the total number of detections for calculating purity:
    sumtot=0

    # Set the necessary parameters before starting:
    if [ $indir = $noisechisel ]; then
        pre="nc"
        magawkcol=4
        suffix="_labeled_o"$txt
    else
        pre="se"
        suffix=$txt
        magawkcol=11
    fi

    # Go over each purity result and put the true detections in
    # `true.txt' and false detections in `false.txt'. Since these
    # files will be appended to, we will delete them if they are
    # already present:
    rm -f true.txt false.txt
    for ind in 1 2 3 4
    do
        # Record the total number of detections:
        catname=$indir/$name$ind$suffix
        tot=$(awk '!/^#/{counter++} END{print counter}' $catname)
        sumtot=$((sumtot+tot))

        # Write the purity values to the TeX macros:
        if   [ $ind = 1 ]; then indlab=a
        elif [ $ind = 2 ]; then indlab=b
        elif [ $ind = 3 ]; then indlab=c
        else                    indlab=d
        fi
        addtexmacro $pre$name$indlab $tot

        # To report the number of false detections for the figure
        # caption:
        if [ $ind = $worstind ]; then
            numfalse=$((tot-25))
            addtexmacro $pre$name"demonfalse" $numfalse
        fi

        # Separate the true and false for the plot:
        awk -i $scripts/checks.awk -v magcol=$magawkcol            \
            -v xcol=$xawkcol -v ycol=$yawkcol                      \
            -f $scripts/puritytf.awk $catname
    done
    addtexmacro $pre$name $(echo " " | \
                                   awk '{printf "%.2f", 100/'$sumtot'}')

    # Find the minimum and maximum of both to make the histogram:
    awk -v out=$pre"truehist"$txt $histopt true.txt
    awk -v out=$pre"falsehist"$txt $histopt false.txt
    rm true.txt false.txt
done

# Merge the histograms into one:
paste nctruehist$txt ncfalsehist$txt setruehist$txt \
      sefalsehist$txt > all.txt
awk '{print $1, $2, $4, $6, $8}' all.txt > $outdir/histplot.txt
rm setruehist$txt sefalsehist$txt
rm all.txt nctruehist$txt ncfalsehist$txt








# CRinnoise.tex
# =============
#
# Figure 15.
fluxlow="-50"
fluxhigh="150"
name="CRinnoise"
outdir=$texdir"/CRinnoise/"
mkdir -p $outdir
astconvertt $images$name$fits --fluxlow=$fluxlow                  \
            --fluxhigh=$fluxhigh --output=$outdir$name$eps
astimgstat --nocfp --histmin=$fluxlow --histmax=$fluxhigh         \
           --histnumbins=100 $images$name$fits                    \
           > $outdir$name$stats$txt
mv $name$hist$txt $outdir

astconvertt $images$name$fits --output=tmp.txt
numhigher=$(awk '!/^#/{for(i=1;i<=NF;++i) if($i>'$fluxhigh') counter++}
                 END{print counter}' tmp.txt)
addtexmacro crinnoisemax $fluxhigh
addtexmacro crinnoisenumhigher $numhigher
rm tmp.txt









# SEback.tex
# ==========
#
# Figure 16. Note that SExtractor's background map for randomsmall,
# onelarge and severallarge is a 200x200 image. Not 1000x1000 like the
# rest.
outdir=$texdir"/SEback/"
mkdir -p $outdir
for name in randomsmall onelarge severallarge sensitivity1      \
                        sensitivity2 sensitivity3 sensitivity4  \
                        onedge 1 2 3 5
do
    # Set the crop box size:
    if [ $name = onedge ]; then
        cropsection="--section=1:201,1:201"
    else
        cropsection=$csection
    fi

    # Just copy the image here if the image is already 200x200.
    if [ $name = randomsmall ] || [ $name = onelarge ] \
           || [ $name = severallarge ]; then
        cp $sextractor$name$back$fits tmp.fits
    else
        astimgcrop $sextractor$name$back$fits $cropsection \
                   --output=tmp.fits
        rm astimgcrop.log
    fi

    # Do the conversion and make the histogram
    astconvertt tmp.fits --output=$outdir$name$eps --borderwidth=5
    astimgstat -q --nocfp --histnumbins=100 tmp.fits
    mv "tmp"$hist$txt $outdir$name$hist$txt
    rm tmp.fits
done










# SEthresh.tex
# ============
#
# Figure 17.
base=onelarge
outdir=$texdir"/SEthresh/"
fluxrange="--fluxlow=$cdnlow --fluxhigh=$cdnNhigh"
apers9095="parameters/mock/onelarge_90_95.txt"
mkdir -p $outdir

# First get the statistics of the unmasked image.
astimgcrop $mock$base$fits $csection
astimgstat $nhc $base$crop$fits > $outdir"notmaskedstats.txt"

# Go over all the thresholds
for thresh in 0.1 0.5 1 2
do
    if [ $thresh == "0.1" ]; then
	conv="--change=1:50000,7934:1"
    elif [ $thresh == "0.5" ]; then
	conv="--change=1:50000,152:1"
    else
	conv=""
    fi

    # Crop and convert the segmentation map:
    astimgcrop $sextractor$base"_t"$thresh$seg$fits $csection
    astconvertt $base"_t"$thresh$seg$crop$fits $conv --fluxlow=0 \
                --fluxhigh=2 --output=$outdir$thresh"_seg"$eps

    # Mask all the detections on the input image, crop it and get its
    # statistics:
    awk $correctfill $sextractor$base"_t"$thresh$txt > tmp.txt
    astmkprof -q tmp.txt $mock$base$fits --oversample=1 \
              --setconsttomin --replace --output=tmp.fits
    astimgcrop tmp.fits $csection
    astimgstat $nhc --ignoremin tmp$crop$fits \
               > $outdir$base"_t"$thresh"_nodet.txt"
    rm tmp.txt tmp$crop$fits

    # Add the elliptical annuli to the 1000x1000 image, then crop it
    # and save it as eps.
    astmkprof $apers9095 tmp.fits --oversample=1 --circumwidth=1 \
              --setconsttomin --replace --output=tmp2.fits
    astimgcrop tmp2.fits $csection --output=tmp.fits
    astconvertt tmp.fits $fluxrange --output=$outdir$thresh"_aper.eps"

    # Clean up:
    rm tmp.fits tmp2.fits
    rm astimgcrop.log astmkprof.log $base"_t"$thresh$seg$crop$fits
done
rm $base$crop$fits










# SEsensitivity.tex
# =================
#
# Figure 18.
outdir=$texdir"/SEsensitivity/"
fluxrange="--fluxlow=$cdnlow --fluxhigh=$cdnhigh"
mkdir -p $outdir
for ind in 1 2 3 4
do
    name=sensitivity$ind

    # First prepare the input image. Note that in getting the
    # statistics of the input image, nothing is masked.
    astimgcrop $mock$name$fits $csection
    astconvertt $name$crop$fits $fluxrange --output=$outdir$name$eps
    astimgstat $nhc $name$crop$fits  \
               > $outdir$name$stats$txt

    astimgcrop $sextractor$name$seg$fits $csection
    astconvertt $name$seg$crop$fits --fluxhigh=1 \
                --output=$outdir$name$seg$eps

    # Mask the detections, convert to eps, then get the statistics,
    # here we want to mask the minimum values.
    awk $correctfill $sextractor$name$txt > tmp.txt
    astmkprof tmp.txt $mock$name$fits --oversample=1 \
              --setconsttomin --replace --output=tmp.fits
    astimgcrop tmp.fits $csection
    astconvertt tmp$crop$fits $fluxrange --output=$outdir$name$aper$eps
    astimgstat $nhc --ignoremin tmp$crop$fits \
               > $outdir$name"_maskedstats.txt"

    # Clean up:
    rm astimgcrop.log astmkprof.log tmp.txt tmp.fits
    rm tmp$crop$fits $name$seg$crop$fits $name$crop$fits
done










# SEreal.tex
# ==========
#
# Figure 19.
outdir=$texdir"/SEreal/"
mkdir -p $outdir
for name in 1 2 3 5 onedge
do
    # Set the constants for each input image:
    if [ $name = onedge ]; then
        inputdir=$mock
        cropsection="--section=1:200,1:200"
        forcorrect=$correctfill
        fluxrange="--fluxlow=$cdnlow --fluxhigh=$cdnmhigh"
    else
        inputdir=$real
        cropsection=$csection
        forcorrect=$correctcircum
        fluxrange="--fluxlow=-0.01 --fluxhigh=0.015"
    fi


    # Similar to the SEsensitivity.tex, we don't need --ignoremin in
    # ImageStatistics here, we need it in the next step when some
    # pixels are masked.
    astimgcrop $inputdir$name$fits $cropsection
    astconvertt $name$crop$fits $fluxrange --output=$outdir$name$eps
    astimgstat $nhc $name$crop$fits > $outdir$name$stats$txt

    astimgcrop $sextractor$name$seg$fits $cropsection
    astconvertt $name$seg$crop$fits --fluxhigh=1 \
                --output=$outdir$name$seg$eps


    # Make the image that will be used in the paper:
    awk $forcorrect $sextractor$name$txt > tmp.txt
    astmkprof tmp.txt $inputdir$name$fits --oversample=1 --replace \
              --setconsttomin --circumwidth=1 --output=tmp.fits
    astimgcrop tmp.fits $cropsection
    astconvertt tmp$crop$fits $fluxrange --output=$outdir$name$aper$eps


    # Get the statistics (by filling the detections).
    awk $correctfill $sextractor$name$txt > tmp.txt
    astmkprof tmp.txt $inputdir$name$fits --oversample=1 --replace \
              --setconsttomin --output=tmp.fits
    astimgcrop tmp.fits $cropsection
    astimgstat $nhc --ignoremin tmp$crop$fits \
               > $outdir$name"_maskedstats.txt"

    # Clean up:
    rm astimgcrop.log astmkprof.log tmp.txt tmp.fits
    rm tmp$crop$fits $name$seg$crop$fits $name$crop$fits
done










# mirrordemo.tex
# ==============
#
# Figure 20.
outdir=$texdir"mirrordemo/"
filename=$noisechisel"1_seg.fits"
histrange="--histmin=-0.005 --histmax=0.01"
astimgcrop $csection $filename --hdu=1 --output=tmp.fits
mkdir -p $outdir
for quant in 10 25 50 75 90
do
    astimgstat -q $nhc $histrange tmp.fits --mirrorquant=0.$quant \
               --histrangeformirror
    mv tmp_mirrorhist.txt $outdir$quant"hist.txt"
    mv tmp_mirrorcfp.txt $outdir$quant"cfp.txt"
done
rm tmp.fits astimgcrop.log










# mode.tex
# ========
#
# Figure 21.
mdist=3
outdir=$texdir/mode/
mkdir -p $outdir
for name in 1 6
do
    indir=$noisechisel
    astimgcrop $indir$name$seg$fits --hdu=1 $csection
    astimgstat $name$seg$crop$fits $nhc --nosigclip --checkmode \
               --noasciihist --mirrorplotdist=$mdist           \
               --histnumbins=150 > $outdir$name$stats$txt
    mv $name"_seg_crop_mode"*".txt" $outdir
    rm $name$seg$crop$fits astimgcrop.log
done

for name in mode1 mode2
do
    if [ $name = mode1 ]; then mdist=4; else mdist=3; fi
    astimgstat $nhc --nosigclip --checkmode --noasciihist --hdu=0 \
               --mirrorplotdist=$mdist $mock$name$fits         \
               --histnumbins=150 > $outdir$name$stats$txt
    mv $name"_mode"*".txt" $outdir
done
