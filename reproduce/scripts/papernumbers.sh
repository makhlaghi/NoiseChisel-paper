#! /bin/bash
#
# papernumbers.sh: Prepare all the necessary numbers for the paper.
#
# The variables not defined in this script are defined in the Makefile
# which calls it. If you want to run this script independently, run it
# once alone and any undefined variable will give an error. For each
# error, specify a value for that variable by running the following
# command. They are usually directories and the names are descriptive.
#
#   export variable=value
#
# Copyright (C) 2015, Mohammad Akhlaghi <akhlaghi@astr.tohoku.ac.jp>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.





# Initial settings:
set -o nounset                  # Stop if a variable is not set.
set -o errexit                  # Stop if a program returns false.
source ./scripts/texmacros.sh   # For setting numbers within the paper.




# Macros:
txt=".txt"
fits=".fits"
conf=".conf"
stats="_stats"
mockparams=$parameters/mock/
gnuastro=$parameters/gnuastro/ast






###################################################
###################################################
# If you want to skip a large portion of the scripts, then uncomment
# the line below and put a `fi' wherever you want to start executing.
#if false; then
###################################################
###################################################


# Add all the SExtractor parameters
awk '!/^#/ && NF>1{
       fixed="source scripts/texmacros.sh && addtexmacro"
       gsub(/_/, "", $1)
       gsub("ASCII_HEAD", "ASCII\\\\\\\\_HEAD", $2)
       command=sprintf("%s %s %s", fixed, $1, $2); system(command)
     }' $parameters/sextractor/sextractor.conf


# Add all the NoiseChisel parameters. Note that nch1 and nch2 will not
# be needed in the paper, also, they have numbers which diqualifies
# them for TeX:
noisechiselconf=$gnuastro"noisechisel"$conf
awk '!/^#/ && NF>1{
       fixed="source scripts/texmacros.sh && addtexmacro"
       if ($1!="nch1" && $1!="nch2")
         command=sprintf("%s %s %s", fixed, $1, $2); system(command)
     }' $noisechiselconf
invqthresh=$(awk '!/^#/ && $1 == "qthresh"{print 100*(1-$2); exit 0}' \
                 $noisechiselconf)
addtexmacro invqthresh $invqthresh
echo "NoiseChisel parameters written"





# Read the time it took to run SCCCD:
filename=$noisechisel/$scccdname$txt
nctime=$(awk '/NoiseChisel finished in/{printf "%.2f", $4}' $filename)
cpughz=$(lscpu | awk '/CPU max MHz:/{printf "%.2f", $4/1000}')
cpunumcore=$(lscpu | awk '/Core\(s\) per socket:/{print $4}')
addtexmacro cpughz $cpughz
addtexmacro nconscccdtime $nctime
addtexmacro cpunumcore $cpunumcore





# Write the SExtractor version:
addtexmacro sextractorversion $sexversion





# Get the proper values:
mpxcol=$(awk '!/^#/ && $1 == "xcol"{print $2; exit 0}' \
             $gnuastro"mkprof"$conf)
mpycol=$(awk '!/^#/ && $1 == "ycol"{print $2; exit 0}' \
             $gnuastro"mkprof"$conf)
mpmcol=$(awk '!/^#/ && $1 == "mcol"{print $2; exit 0}' \
             $gnuastro"mkprof"$conf)
mpqcol=$(awk '!/^#/ && $1 == "qcol"{print $2; exit 0}' \
             $gnuastro"mkprof"$conf)
mpncol=$(awk '!/^#/ && $1 == "ncol"{print $2; exit 0}' \
             $gnuastro"mkprof"$conf)
mprcol=$(awk '!/^#/ && $1 == "rcol"{print $2; exit 0}' \
             $gnuastro"mkprof"$conf)
mpfcol=$(awk '!/^#/ && $1 == "fcol"{print $2; exit 0}' \
             $gnuastro"mkprof"$conf)
mptcol=$(awk '!/^#/ && $1 == "tcol"{print $2; exit 0}' \
             $gnuastro"mkprof"$conf)





# Write the data for the mode appendix:
mirrordist=$(awk '!/^#/ && /mirrordist/ {print $2}' \
                 $gnuastro"imgstat"$conf)
addtexmacro imgstatmirrordist $mirrordist
for ind in 1 2
do
    # Set the output base name
    if [ $ind = 1 ]; then obase=one
    else                  obase=two
    fi

    # Get the basic profile parameters:
    x=$(awk '!/^#/{print $('$mpxcol'+1)}' $mockparams/mode$ind$txt)
    y=$(awk '!/^#/{print $('$mpycol'+1)}' $mockparams/mode$ind$txt)
    mag=$(awk '!/^#/{print $('$mpmcol'+1)}' $mockparams/mode$ind$txt)
    n=$(awk '!/^#/{print $('$mpncol'+1)}' $mockparams/mode$ind$txt)
    re=$(awk '!/^#/{print $('$mprcol'+1)}' $mockparams/mode$ind$txt)
    t=$(awk '!/^#/{print $('$mptcol'+1)}' $mockparams/mode$ind$txt)
    addtexmacro mode$obase"x"   $x
    addtexmacro mode$obase"y"   $y
    addtexmacro mode$obase"mag" $mag
    addtexmacro mode$obase"n"   $n
    addtexmacro mode$obase"re"  $re
    addtexmacro mode$obase"t"   $t
done

# Get the necessary values for the mode plot
for name in 1 6 mode1 mode2
do
    if   [ $name = 1 ];     then obase=realone; multip=10000;
    elif [ $name = 6 ];     then obase=realsix; multip=10000;
    elif [ $name = mode1 ]; then obase=modeone; multip=1;
    elif [ $name = mode2 ]; then obase=modetwo; multip=1;
    fi

    filename=$texdir/mode/$name"_stats"$txt
    sym=$(awk '/Mode symmetricity/ && NF==9 {printf "%.2f", $8}' \
              $filename)
    modeq=$(awk '/Mode/ && NF==6 {printf "%.2f", 100*$5}' $filename)
    addtexmacro mode$obase"sym" $sym
    addtexmacro mode$obase"modeq" $modeq

    mode=$(awk '/Mode/ && NF==6 {print $6}' $filename)
    symv=$(awk '/Mode symmetricity/ && NF==9 {print $9}' $filename)
    sympoint=$(echo " " | awk '{print ('$symv'-'$mode')*'$multip'}')
    addtexmacro mode$obase"sympoint" $sympoint


done


# Report the number of false detections with different SExtractor
# thresholds and also the fraction of the total flux within the
# profile.
sefacol=5
seareacol=7
fullflux=$(awk '/Sum/ && NF==3 {print $3}' $mock/onelarge_fullflux$txt)
for thresh in 0.1 0.5 1 2
do
    # Set the TeX macro name:
    if [ $thresh = 0.1 ]; then obase=tenth
    elif [ $thresh = 0.5 ]; then obase=half
    elif [ $thresh = 1 ]; then obase=one
    elif [ $thresh = 2 ]; then obase=two
    fi

    # Find the number of false detection (there is only one true
    # detection!) and report it:
    filename=$sextractor/onelarge_t$thresh$txt
    num=$(awk '!/^#/{counter++} END{print counter-1}' $filename)
    addtexmacro se$obase"thresh" $num

    # Find the total flux:
    fluxauto=$(awk '!/^#/ && $'$seareacol'>1000{print $'$sefacol'}'\
                   $filename)
    frac=$(echo " " | awk '{printf "%.2f", 100*'$fluxauto'/'$fullflux'}')
    addtexmacro se$obase"kronff" $frac
done



# Report the number of SExtractor false detections with different
# DETEC_MINAREA:
thresh=0.1
for minarea in 10 25 50 75 100
do
    if [ $minarea = 10 ]; then obase=ten
    elif [ $minarea = 25 ]; then obase=twofive
    elif [ $minarea = 50 ]; then obase=fifty
    elif [ $minarea = 75 ]; then obase=sevenfive
    elif [ $minarea = 100 ]; then obase=hundred
    fi

    # Find the number of false detection (there is only one true
    # detection!) and report it:
    filename=$sextractor/onelarge_t$thresh"_m"$minarea$txt
    num=$(awk '!/^#/{counter++} END{print counter-1}' $filename)
    addtexmacro se$obase"minarea" $num
done




# Get the background value for later, it has already been saved in the
# TeX macro file in paperfigures.sh
mknoiseconf=$gnuastro"mknoise"$conf
zeropoint=$(awk '!/^#/ && $1 == "zeropoint"{print $2; exit 0}' \
                $mknoiseconf)
backmag=$(awk '!/^#/ && $1 == "background"{print $2; exit 0}'  \
              $mknoiseconf)
backcount=$(echo " " | awk '{
    print 10^( ('$backmag'-'$zeropoint')/-2.5 )}')




# Read the mode of the three examples of dataandnoise:
function writeimgstatmode
{
    filename=$texdir/dataandnoise/$name"_stats"$txt
    mode=$(awk '/Mode \(quantile, value\)/ \
                {printf("%.2f", $6-'$backcount')}' $filename)
    sym=$(awk '/Mode symmetricity and/ {printf "%.2f", $8}' $filename)
    addtexmacro txt$name"mode" $mode
    addtexmacro txt$name"modesym" $sym
}
for name in randomsmall onelarge severallarge
do writeimgstatmode; done






# Find the kron mode approximation:
filename=$texdir"dataandnoise/onelarge_crop_hist"$txt
awk '!/^#/' $filename | sort -n -k2 | tail --lines=7 > top.txt
kronmean=$(awk '{sum+=$1; num++} END{printf "%.2f", sum/num}' top.txt)
kronstd=$(awk '{sum+=(($1-'$kronmean')*($1-'$kronmean')); num++}
                END{printf "%.2f", sqrt(sum/num)}' top.txt)
kronmean=$(echo $kronmean | awk '{print $1-'$backcount'}')
addtexmacro onelargekronstd $kronstd
addtexmacro onelargekronmean $kronmean
rm top.txt




# Read the multiple and number of sigma clippings:
imgstatconf=$gnuastro"imgstat"$conf
issigclipnum=$(awk '!/^#/ && $1 == "sigclipnum"{print $2; exit 0}' \
                 $imgstatconf)
issigclipmultip=$(awk '!/^#/ && $1 == "sigclipmultip"{print $2; exit 0}' \
                    $imgstatconf)
issigcliptolerance=$(awk '!/^#/ && $1 == "sigcliptolerance"{print $2
                          exit 0}' $imgstatconf)
addtexmacro issigclipnum $issigclipnum
addtexmacro issigclipmultip $issigclipmultip
addtexmacro issigcliptolerance $issigcliptolerance
echo "ImageStatistics parameters written."


# MakeProfiles:
mkprofconf=$gnuastro"mkprof"$conf
naxisa=$(awk '!/^#/ && $1 == "naxis1"{print $2; exit 0}' \
             $mkprofconf)
naxisb=$(awk '!/^#/ && $1 == "naxis1"{print $2; exit 0}' \
             $mkprofconf)
addtexmacro naxisa $naxisa
addtexmacro naxisb $naxisb
echo "MakeProfiles parameters written."





# SExtractor:
seconf=$parameters/sextractor/sextractor.conf
rkron=$(awk '/PHOT_AUTOPARAMS/{print $2}' $seconf)
addtexmacro rkron ${rkron%?}






# Find the minimum and maximum magnitude in the random profiles:
function readmkprofcat
{
    all=$(awk 'BEGIN{gminmag=9999999; gmaxmag=-gminmag}
               !/^#/{
                 mag=$('$mpmcol'+1);
                 if($('$mpfcol'+1)==0) {
                   gal++
                   if(mag<gminmag) gminmag=mag
                   if(mag>gmaxmag) gmaxmag=mag
                 }
                 if($('$mpfcol'+1)==3) {
                   star++
                   starmag=mag
                 }
               }
               END{printf("%d  %.0f %d %.0f %.0f", star,
                          starmag, gal, gminmag, gmaxmag)}' $1)
}



readmkprofcat $mockparams/randomsmall$txt
addtexmacro datanoisersnstars   $(echo $all | awk '{print $1}')
addtexmacro datanoisersstarmag  $(echo $all | awk '{print $2}')
addtexmacro datanoisersngals    $(echo $all | awk '{print $3}')
addtexmacro datanoisersgminmag  $(echo $all | awk '{print $4}')
addtexmacro datanoisersgmaxmag  $(echo $all | awk '{print $5}')

readmkprofcat $mockparams/severallarge$txt
addtexmacro datanoiseslngals    $(echo $all | awk '{print $3}')
addtexmacro datanoiseslgminmag  $(echo $all | awk '{print $4}')
addtexmacro datanoiseslgmaxmag  $(echo $all | awk '{print $5}')

onelargemag=$(awk '!/^#/{print $('$mpmcol'+1)}' $mockparams/onelarge$txt)
onelargen=$(awk '!/^#/{print $('$mpncol'+1)}' $mockparams/onelarge$txt)
onelargere=$(awk '!/^#/{print $('$mprcol'+1)}' $mockparams/onelarge$txt)
onelargeq=$(awk '!/^#/{print $('$mpqcol'+1)}' $mockparams/onelarge$txt)
addtexmacro onelargemag $onelargemag
addtexmacro onelargen $onelargen
addtexmacro onelargere $onelargere
addtexmacro onelargeq $onelargeq
echo "Profile parameters of Figure 1, written."

function readall5sigclips
{
    all=$(awk '{
                if($0 ~ /4.00 sigma-clipping '$issigclipnum'/)
                  start=1
                else if (start==1 && $0 !~ /ImageStatistics finished in/)
                  printf("%.2f  ", $2-'$backcount')
               }' $1$2$stats$txt)

    addtexmacro $2"sca" $(echo $all | awk '{print $1}')
    addtexmacro $2"scb" $(echo $all | awk '{print $2}')
    addtexmacro $2"scc" $(echo $all | awk '{print $3}')
    addtexmacro $2"scd" $(echo $all | awk '{print $4}')
    addtexmacro $2"sce" $(echo $all | awk '{print $5}')
}
readall5sigclips $texdir/dataandnoise/ randomsmall
readall5sigclips $texdir/dataandnoise/ onelarge
readall5sigclips $texdir/dataandnoise/ severallarge
echo "Sigma-clipped results for Figure 1, written."





# Numbers for dettf.tex:
sqnt=9999999
function dettfhist
{
    # Set the file name.
    if [ $2 == 4 ]; then                obase=four;
    elif [ $2 = sensitivity3 ]; then    obase=sensitivityc;
    else                                obase=$2;
    fi
    if [ $2 == onelarge ]; then ind="_7"; else ind="_12"; fi
    name=$1$2$ind"_detsn"$txt

    dettfnum=$(awk '/points binned in/{print $4; exit(0)}' $name)
    dettfqnt=$(awk '/quantile has a value of/{
                      printf("%.2f", $9); exit(0);}' $name)
    dettfmax=$(awk 'BEGIN { max=-999999 }
                    !/^#/ { if($2>max){max=$2; mv=$1} }
                    END { printf("%.2f", mv) }' $name)
    addtexmacro $obase"dettfnum" $dettfnum
    addtexmacro $obase"dettfmax" $dettfmax
    addtexmacro $obase"dettfqnt" $dettfqnt

    # Find the smallest S/N quantile:
    sqnt=$(echo " " | awk '{if('$dettfqnt'<'$sqnt') print '$dettfqnt'}')
}
for base in 4 onelarge sensitivity3
do dettfhist $texdir/dettf/ $base;  done
addtexmacro dettfsmallestsnqnt $sqnt


# Size of the SCCCD image:
naxis1=$(astheader $SCCCD --hdu=1 | grep NAXIS1 | awk '{print $3}')
naxis2=$(astheader $SCCCD --hdu=1 | grep NAXIS2 | awk '{print $3}')
addtexmacro scccdnaxisa $naxis1
addtexmacro scccdnaxisb $naxis2



# onedge:
all=$(awk '!/^#/ && NF>1{counter++;
                 if(counter==2)print -1*$2, $7}' $parameters/mock/onedge.txt)
addtexmacro onedgepa $(echo $all | awk '{print $2}')
addtexmacro onedgedist $(echo $all | awk '{print $1}')



# analysistable.tex:

function prepsensitivity
{
    if [ $1 == sensitivity1 ]; then obase=sena;
    elif [ $1 == sensitivity2 ]; then obase=senb;
    elif [ $1 == sensitivity3 ]; then obase=senc;
    elif [ $1 == sensitivity4 ]; then obase=send;
    fi

    # Set input names:
    ncmask=$texdir/NCsensitivity/$1"_stats"$txt
    senomask=$texdir/SEsensitivity/$1"_stats"$txt
    catfile=$texdir/NCsensitivity/$1"_labeled_o"$txt
    semask=$texdir/SEsensitivity/$1"_maskedstats"$txt

    # Set the constants:
    multip=1
    sub=$backcount
}

function preponelarge
{
    obase=onelarge

    # Set input names:
    ncmask=$texdir/det/onelarge_stats$txt
    catfile=$texdir/det/onelarge_labeled_o$txt
    senomask=$texdir/SEthresh/notmaskedstats$txt
    semask=$texdir/SEthresh/onelarge_t2_nodet$txt

    # Set the constants:
    multip=1
    sub=$backcount
}

function prepreal
{
    if [ $1 == 1 ]; then obase=reala;
    elif [ $1 == 2 ]; then obase=realb;
    elif [ $1 == 3 ]; then obase=realc;
    elif [ $1 == 5 ]; then obase=reald;
    else obase=$1
    fi

    # Set input names:
    ncmask=$texdir/NCreal/$1"_stats"$txt
    senomask=$texdir/SEreal/$1"_stats"$txt
    semask=$texdir/SEreal/$1"_maskedstats"$txt

    # Set the constants:
    if [ $1 = onedge ]; then
        multip=1
        sub=$backcount
        catfile=$texdir/NCreal/$1"_labeled_o"$txt
    else
        sub=0
        catfile=none
        multip=10000
    fi
}

function sigmaclipconverge
{
    # Arguments named for clairty, we don't want the global variables
    # to get mixed, so there a `t' appended to them:
    tsub=$2
    tmultip=$3
    tfilename=$1

    # Find the values:
    all=$(awk '{
      if($0 ~ /times sigma by convergence/) start=1
      else if(start==1 && $0 ~ /sigma-clipping '$issigclipnum' times/)
        exit(0);
      else if(start==1) {mean='$tmultip'*$3-'$tsub'; err='$tmultip'*$4}
    }
    END{printf("%.1f %.1f", mean, err)}' $tfilename)

    # Separate them:
    scconv=$(echo $all | awk '{print $1}')
    scconverr=$(echo $all | awk '{print $2}')
}

function analysistablerow
{
    # Get the initial median and mode:
    med=$(awk '/Median/ && NF==3 {printf("%.1f", '$multip'*$3-'$sub')
                                  exit(0)}' $senomask)
    mode=$(awk '/Mode/ && NF==6 {printf("%.1f", '$multip'*$6-'$sub')
                                 exit(0)}' $senomask)
    modecheck=$(awk '/SYMMETRICITY/{print "^{LS}"; exit(0);}' $senomask)



    # Sigma-clipping convergence:
    sigmaclipconverge $senomask $sub $multip


    # Sigma-clipping by fixed number:
    all=$(awk '{
      if($0 ~ /sigma-clipping '$issigclipnum' times:/) start=1
      else if(start==1 && $0 ~ /ImageStatistics finished/) exit(0);
      else if(start==1) {median='$multip'*$2-'$sub'; err='$multip'*$4}
    }
    END{printf("%.1f %.1f", median, err)}' $senomask)
    scfixed=$(echo $all | awk '{print $1}')
    scfixederr=$(echo $all | awk '{print $2}')



    # SExtractor and NoiseChisel average of undetected:
    semean=$(awk '/Mean/ && NF==3 {
                   printf("%.1f", '$multip'*$3-'$sub');
                   exit(0) }' $semask)
    seerr=$(awk '/deviation/ && NF==4 {
                   printf("%.1f", '$multip'*$4); exit(0)}' $semask)
    ncmean=$(awk '/Mean/ && NF==3 {
                   printf("%.1f", '$multip'*$3-'$sub'); exit(0)}' $ncmask)
    ncerr=$(awk '/deviation/ && NF==4 {
                   printf("%.1f", '$multip'*$4); exit(0)}' $ncmask)


    # Write all the macros so far:
    addtexmacro $obase"med"     $med
    addtexmacro $obase"mode"    $mode$modecheck
    addtexmacro $obase"scconv"  $scconv"\\\\pm"$scconverr
    addtexmacro $obase"scfixed" $scfixed"\\\\pm"$scfixederr
    addtexmacro $obase"semean"  $semean"\\\\pm"$seerr
    addtexmacro $obase"ncmean"  $ncmean"\\\\pm"$ncerr



    # Find number of false detections:
    if [ $catfile != none ]; then

        # The galaxies in `onedge' are not in the desired region of
        # the image so we can't use the puritytf.awk script. However,
        # the advantage of onedge is that all detections are very
        # bright. So we can use a magnitude limit to find false
        # detections:
        if [ $catfile = $texdir/NCreal/"onedge_labeled_o"$txt ]; then
            numfalse=$(awk '!/^#/{if($'$magawkcol'>-10) counter++}
                            END{print counter}' $catfile)
        else
            rm -f true.txt false.txt # Just to make sure.
            awk -i $scripts/checks.awk -v magcol=$magawkcol     \
                -v xcol=$xawkcol -v ycol=$yawkcol               \
                -f $scripts/puritytf.awk $catfile
            numfalse=$(wc -l false.txt | awk '{print $1}')
            rm true.txt false.txt
        fi

        # Write the macro:
        addtexmacro $obase"nfalse"  $numfalse
    fi
}

function onlyseanalysisrow
{
    semask=$texdir"/SEthresh/onelarge_t"$1"_nodet"$txt

    if [ $1 = 1 ];     then obase=onelargetone
    elif [ $1 = 0.5 ]; then obase=onelargethalf
    else                    obase=onelargettenth
    fi

    semean=$(awk '/Mean/ && NF==3 {
                   printf("%.1f", '$multip'*$3-'$sub'); exit(0)}' $semask)
    seerr=$(awk '/deviation/ && NF==4 {
                   printf("%.1f", '$multip'*$4); exit(0)}' $semask)
    addtexmacro $obase"semean"  $semean"\\\\pm"$seerr
}

# Set the variables:
xawkcol=2
yawkcol=3
magawkcol=4

for ind in 1 2 3 4;
do
    prepsensitivity sensitivity$ind
    analysistablerow

    # To find the fractions reported in the text:
    if [ $ind = 1 ]; then
        frac=$(echo " " | awk '{printf "%.1f", '$semean'/'$ncmean'}')
        addtexmacro senasencfrac  $frac
    fi
done
for name in 1 2 3 5 onedge
do
    prepreal $name
    analysistablerow

    # To find the fractions reported in the text:
    if [ $name = onedge ]; then
        frac=$(echo " " | awk '{printf "%.1f", '$semean'/'$ncmean'}')
        addtexmacro onedgesencfrac  $frac
    fi
done
preponelarge
analysistablerow

# To find the fractions reported in the text:
frac=$(echo " " | awk '{printf "%.1f", '$semean'/'$ncmean'}')
addtexmacro onelargettsencfrac  $frac

for thresh in 1 0.5 0.1
do
    onlyseanalysisrow $thresh

    # To find the fractions reported in the text:
    if [ $thresh = 1 ]; then
        frac=$(echo " " | awk '{printf "%.1f", '$semean'/'$ncmean'}')
        addtexmacro onelargetosencfrac  $frac
    fi
done
echo "Analysis table values written."





# Report the SExtractor mean after sigma-clipping convergence:
for name in randomsmall onelarge severallarge
do
    filename=$texdir/dataandnoise/$name"_stats"$txt

    # The median:
    med=$(awk '/Median/ && NF==3 {printf("%.1f", $3-'$backcount')
                                  exit(0)}' $filename)
    addtexmacro dnintxt$name"med" $med

    # Sigma clipping by convergence
    sigmaclipconverge $filename $backcount 1
    addtexmacro dnintxt$name"scconv" $scconv"\\\\pm"$scconverr

    # Initial Standard deviation:
    firststd=$(awk '{if($0 ~ /times sigma by convergence/) start=1
         else if(start==1) {printf("%.2f", $4); exit(0);}}' $filename)
    addtexmacro dnintxt$name"scfstd" $firststd
done
echo "Figure 1 values in text of Appendix, written."
