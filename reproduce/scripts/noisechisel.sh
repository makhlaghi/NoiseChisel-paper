#! /bin/bash
#
# noisechisel.sh: Run NoiseChisel on the images.
#
# The variables not defined in this script are defined in the Makefile
# which calls it. If you want to run this script independently, run it
# once alone and any undefined variable will give an error. For each
# error, specify a value for that variable by running the following
# command. They are usually directories and the names are descriptive.
#
#   export variable=value
#
# Copyright (C) 2015, Mohammad Akhlaghi <akhlaghi@astr.tohoku.ac.jp>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.





# Initial settings:
set -o nounset                  # Stop if a variable is not set.
set -o errexit                  # Stop if a program returns false.
source ./scripts/texmacros.sh   # For setting numbers within the paper.




# Macros:
txt=".txt"
fits=".fits"





# Make the output directory:
mkdir -p $noisechisel





# Purity test
# ===========
#
# Why is purity the first thing NoiseChisel runs on?
#
# Because, when you are doing the test, you can simply put an `exit 0'
# line after this test and also after the test on SExtractor. Then
# simply by running `make' from the top `Reproduce' directory, you can
# test NoiseChisel and SExtractor's results in one command without
# wasting too much time.
name=purity
for ind in 1 2 3 4
do
    astnoisechisel $mock$name$ind$fits --checkmaskdet
    astmkcatalog $name$ind"_labeled"$fits
    mv $name$ind"_labeled_o"$txt $noisechisel
    mv $name$ind"_maskdet"$fits $noisechisel$name$ind"_maskdet"$fits
    rm $name$ind"_labeled"$fits $name$ind"_labeled_c"$txt
done





# onelarge.fits
# ============
name=onelarge
astnoisechisel $mock$name$fits --checkdetection --checksegmentation   \
               --checkmaskdet --detsnhistnbins=50 --segsnhistnbins=50 \
               --checkthresh
astmkcatalog $name"_labeled"$fits
mv $name"_labeled_o"$txt $noisechisel
mv $name"_thresh"$fits $name"_seg"$fits $noisechisel
mv $name"_det"$fits $name"_maskdet"$fits $noisechisel
mv $name"_7_detsn"$txt $name"_12_segsn"$txt $noisechisel
rm *_detsn$txt *_segsn$txt $name"_labeled"$fits $name"_labeled_c"$txt





# sensitivity2 sensitivity3 4.fits
# ===========================
for name in sensitivity2 sensitivity3 4
do
    # Preparations:
    if [ $name = 4 ]; then
        filename=$real$name$fits
        skysubed="--skysubtracted"
    else
        filename=$mock$name$fits
        skysubed=" "
    fi

    # Run NoiseChisel:
    if [ $name = sensitivity2 ]; then
        astnoisechisel $filename --checkdetection --checkmaskdet \
                       --checkthresh
    else
        astnoisechisel $filename --checkdetection --checkmaskdet \
                       --detsnhistnbins=50 --checkthresh $skysubed
        mv $name"_12_detsn.txt" $noisechisel
        rm *_detsn.txt
    fi
    mv $name"_thresh"$fits $noisechisel
    mv $name"_det"$fits $name"_maskdet"$fits $noisechisel

    # Generate catalogs for the mock cases (we want the number of
    # false detections for the table):
    if [ $name = sensitivity2 ] || [ $name = sensitivity3 ]; then
        astmkcatalog $name"_labeled"$fits
        mv $name"_labeled_o"$txt $noisechisel
        rm $name"_labeled_c"$txt
    fi
    rm $name"_labeled"$fits
done






# 1.fits 3.fits 5.fits
# ====================
#
# For these three we want the object labels to be deterministic, so
# NoiseChisel has to be run on one thread. When using Multiple
# threads, there is no telling which thread the objects will fall into
# and so, the labels will differ each time.
for name in 1 3 5
do
    astnoisechisel $real$name$fits --checksegmentation --numthreads=1 \
                   --segsnhistnbins=50 --skysubtracted --checkmaskdet
    mv $name"_seg"$fits $name"_maskdet.fits" $name"_12_segsn.txt" $noisechisel
    rm $name"_labeled"$fits *"_segsn"$txt
done





# 6.fits
# ======
name=6
astnoisechisel $real$name$fits --checksegmentation --skysubtracted
mv $name"_seg"$fits $noisechisel
rm $name"_labeled"$fits





# sensitivity1 sensitivity4 2 onedge
for name in sensitivity1 sensitivity4 2 onedge
do
    if [ $name = 2 ]; then
        skysubed="--skysubtracted"
        fullname=$real$name$fits
    else
        skysubed=" "
        fullname=$mock$name$fits
    fi
    astnoisechisel $fullname $skysubed --checkmaskdet
    mv $name"_maskdet"$fits $noisechisel

    # Generate catalogs for the mock cases (we want the number of
    # false detections for the table):
    if [ $name != 2 ]; then
        astmkcatalog $name"_labeled"$fits
        mv $name"_labeled_o"$txt $noisechisel
        rm $name"_labeled_c"$txt
    fi
    rm $name"_labeled"$fits
done




# SCCCD.fits
# ==========
scccdncha=4
scccdlmesh=256
echo "NoiseChisel running on $SCCCD, output is redirected..."
astnoisechisel $SCCCD --hdu=1 --mhdu=2 --nch1=$scccdncha          \
               --fullinterpolation  --fullsmooth --meshbasedcheck \
               --checkmaskdet --checksky --lmesh=$scccdlmesh      \
               --saveskysubed > $noisechisel$scccdname$txt
mv $scccdname"_maskdet"$fits $scccdname"_sky"$fits $noisechisel
mv $scccdname"_skysubed"$fits $scccdname"_labeled"$fits $noisechisel
addtexmacro scccdncha $scccdncha
addtexmacro scccdlmesh $scccdlmesh
