# texmacros.sh: Function to add a macro to the TeX macros file.
#
# This function adds a macro to a given file name. If the TeX macro
# file doesn't already exist, it will be created. If it exists, it
# will be searched a if a macro with the given name exists, it will be
# re-written.
#
# Copyright (C) 2015, Mohammad Akhlaghi <akhlaghi@astr.tohoku.ac.jp>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.
#
#
# Variables:
#    texmacro
#
# Arguments:
#    1. Macro name.
#    2. Macro value.
function addtexmacro()
{

    # If file didn't exist, then make a file.
    if [ ! -f $texmacro ]; then
        touch $texmacro
    fi

    # Set a stupid temporary name:
    TMPNAME="abcdefghijklmnopqrstuvwxyz.abc"

    # Check if a line with this macro exists. If it does, then change
    # it, if it doesn't add it.
    awk '{ if($0 !~ /\{\\'$1'\}/) print > "'$TMPNAME'" }
         END { print "\\newcommand{\\'$1'}{'$2'}" > "'$TMPNAME'" }' \
        $texmacro

    mv $TMPNAME $texmacro
}
