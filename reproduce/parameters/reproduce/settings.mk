# Reproduction settings
# =====================
#
# The variables defined here are read by the Makefile and specify the
# input directories and some operation steps.
#
# We are following the GNU convention on Makefile variable
# names. Namely, variables that are to be set by the user are written
# in an all upper-case style. While internal variables are
# small-caps. So this file only contains upper-case variables. Within
# the Makefile and scripts, you will notice lots of internal
# small-caps variables.
#
# Everything is thoroughly explained in the README file, please read
# that file first then set the variables here.
#
# These constants can be specified on the command line as environment
# variables without modifying this file, for example:
#
#    export TMPDIR=/mnt/Work/NoiseChiselTest/
#    export CUTOUTS=/another/directory
#    make -e
#
# With the `-e' option, the environment variables takes precedence
# over the variables defined here.
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.




# Already made cutouts
# ====================
#
# If you already have cutout images (using the IRSA cutout tool as
# explained in README), then please specify their directory here. The
# scripts expect 6 images labeled 1.fits, 2.fits until 6.fits within
# this directory.
#
# If these cutouts are not present, then the script will use
# Gnuastro's ImageCrop utility to crop out the targets from the survey
# tiles within the COSMOSACS directory specified below.
CUTOUTS=/mnt/Work/NoiseChiselInputs/





# COSMOS HST-ACS survey tiles
# ===========================
#
# The input survey images have to be in the following directory. See
# README for more information. They can be downloaded from
#
# http://irsa.ipac.caltech.edu/data/COSMOS/images/acs_mosaic_2.0/tiles/
#
# If the cutout images are available in the CUTOUTS directory, this
# directory will never be used. If there are no cutouts and the
# objects aren't within this directory, then an error will be raised.
COSMOSACS=/mnt/Work/COSMOSACSF814W/





# Subaru SuprimeCam image:
# ========================
#
# Please download the image from the following address and specify its
# absolute address as the value to this variable:
#
# http://astr.tohoku.ac.jp/~akhlaghi/reproduce/1505.01664/CORR01269690.fits
SCCCD=/mnt/Work/NoiseChiselInputs/CORR01269690.fits



# Temporary working directory
# ===========================
#
# TMPDIR is the directory that will host all the (temporary or check)
# outputs of the different steps of this reproduction. Since the
# images are large and multiple copies of them in various stages will
# stored, TMPDIR should be within a partition with a large amount of
# free space (>2 GBytes) and with write-access by the user running
# this command. All the image files of the various steps will be
# placed in respective directories for inspection and will not be
# deleted by default. After the processing finishes, you can safely
# remove this whole directory by running `make clean'.
#
# The basic idea behind leaving the place of this directory to the
# user is that the temporary check images will take a lot of space and
# this will cause lots of problems if mixed with the user's main
# (important) directories. For example if the important files are
# synced or backed up across multiple machines, the presence of these
# numerous and large temporary images will be a serous burden.
TMPDIR=/mnt/Work/NoiseChiselPaper
