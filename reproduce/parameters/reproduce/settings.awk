# These values are read to create the mock profiles the Akhlaghi and
# Ichikawa (2015) ApJS. 220, 1. arXiv: 1505:01664.
#
# Changing the values here will create mock images with these
# parameters and also the numbers reported in the text of the paper
# will also be respectively changed. Therefore before changing these
# important variables, it is best to first read the paper to
# understand their significance.
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

BEGIN{

    demofwhms[0]=1   # First FWHM of convolution demonstration.

    demofwhms[1]=3   # Second FWHM of convolution demonstration.

    demofwhms[2]=5   # Third FWHM of convolution demonstration.

    demofwhms[3]=10  # Fourth FWHM of convolution demonstration.

    demofwhms[4]=20  # Fifth FWHM of convolution demonstration.

    ns[0]=0.5        # First sensitivity test Sersic index.

    ns[1]=1          # Second sensitivity test Sersic index.

    ns[2]=4          # Third sensitivity test Sersic index.

    ns[3]=10         # Fourth sensitivity test Sersic index.

    mockfwhm=3       # FWHM of Moffat function for mock imags.

    moffatbeta=4.76  # Moffat beta value to use for PSF.

    senq=0.5         # Axis ratio of Sensitivity test profiles.

    senre=10         # Effective radius of Sensitivity test profiles.

    truncr=5         # Truncation radius of Sensitivity test profiles.

    sentheta=45      # Position angle of Sensitivity test profiles.

    purityn=0.5      # Sersic index of purity test.

    puritymag=-10.5  # Magnitude of purity test.

    senfaintmag=-9.2 # Faint magnitude in Sensitivity test profiles.

    senmagdiff=-0.06 # Diff. in magnitude in Sensitivity test profiles.
}
