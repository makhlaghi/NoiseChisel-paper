# Reproduction details

This file contains more details on the reproduction process that were
not in the top `README.md` file.

## Configuration files and parameters

All the major file and directory variables are within the
`parameters/reproduce/settings.mk` file. The comments above each
variable in that file explain the effect of that variable on the
script.

The major mock profile parameters can be set within the
`parameters/reproduce/settings.awk` file. These parameters are read by
the script which generates the mock catalogs which are then turned
into images and processed by other programs. Having read the paper,
the comments of each variable should be indicative enough of what they
do.


### Updating figures

The plots are made with the PGFPlots package within LaTeX. If the
figures are already present within a `tikz/` directory (you have
already created the PDF paper once since the `tikz/` directory is made
by the top level `Makefile`), and the source files have not changed,
the same figures will be used for the next LaTeX compilation. So if
you have already run the reproduction system once and decide to check
how a change of parameters would affect the figures, then remove the
`tikz/` directory before running Make in the top paper directory.

```
$ rm -rf ./tikz/
```

If you only want to check the numbers, this is not needed. Also if you
already know the figure you want to check, then in order to speed up
the process, you can only delete the TikZ files related to that
figure. For example if only Figure 1 is desired, then run (note that
TikZ counting starts from 0):

```
$ rm ./tikz/paper-figure0.*
```

### Last column of Figure 10

The color coding of the images of the last column of Figure 10 are
specifically defined for NoiseChisel with the published set of input
parameters. If you are using different parameters, they will become
black. Please set the numbers by hand in `scripts/paperfigures.sh` as
explained in the comments above the part of this script for Figure 10.










## Necessary data (FITS) files

### COSMOS galaxies

The real galaxy images in this paper are from the COSMOS HST/ACS
images. If the science image tiles are available on your system or as
a local network address which you can access on the command-line like
a regular file, set the address of the directory containing the FITS
images as the value of the COSMOSACS variable in
`parameters/reproduce/settings.mk`.

The full archive of image tiles over this very wide field can be
downloaded from [The IRSA COSMOS
archives](http://irsa.ipac.caltech.edu/data/COSMOS/images/acs_mosaic_2.0/tiles/). Note
that only the science images are necessary. The science images of all
the tiles are roughly 136 Gigabytes! So if you don't already have
them, and you only want to reproduce the results of this paper, it is
easier to follow the alternative path below.

Alternatively, you can get crops from the cutout tool at the survey
webpage. The coordinates of the real galaxies used are listed in
`parameters/real/positions.txt`.

This file is formatted in the ASCII "IPAC table" format which you can
feed into the [online cutout tool for the COSMOS
survey](http://irsa.ipac.caltech.edu/data/COSMOS/index_cutouts.html). Set
the size of the images to **30** arcseconds = 1000 pixels and choose
the **HST-ACS Mosaic** in the "COSMOS Data Sets" table.

There are multiple options once the download is complete, please
follow which ever method you like (the `wget` command might the
easiest, just run the displayed command in the directory you want to
keep the images and the FITS images will be put there). The outputs
are ordered in the same order of the table, so simply rename them to
`1.fits`, `2.fits`, until `6.fits`. Then set the directory which keeps
these fits images as the value of the CUTOUTS variable in
`parameters/reproduce/settings.mk`.



### Subaru Suprime-cam image

Figure 11 uses one full CCD image recorded by Subaru Suprime-cam.
This image can be downloaded from the following URL:
http://astr.tohoku.ac.jp/~akhlaghi/reproduce/1505.01664/CORR01269690.fits

Once you have downloaded it where ever you like, put its full file
name as the value of the SCCCD variable in
`parameters/reproduce/settings.mk`.










## Contents

This directory initially contains two directories (below) and two
files. The files are this `README.md` file and the `Makefile` that
organizes the execution of all the reproduction scripts. The
directories are explained below.


### parameters directory

As the `parameters` name suggests, this directory keeps all the
parameters necessary to create the mock and real input images, the
parameters necessary for SExtractor and the GNU Astronomy Utilities
and also the parameters to run this reproduction system. You can
modify them and see their effect on the produced paper.


### scripts directory

The shell scripts in the `scripts` directory are the main work horse
of this reproduction system. They are called by the Makefile when
needed, their names are very descriptive. To see the order in which
they are run, please see the Makefile. If you search the term `.sh` in
the Makefile from the top of the file, you can see the "rules" which
call the scripts. The `Makefile` in this directory is heavily
commented, so you don't necessarily have to know the Makefile syntax
to understand what is going on.
