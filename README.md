# Reproduction pipeline for the NoiseChisel paper

[![JournalDOI](http://astr.tohoku.ac.jp/~akhlaghi/reproduce/1505.01664/ApJS_220_1.svg)](http://dx.doi.org/10.1088/0067-0049/220/1/1)
[![arXiv](http://astr.tohoku.ac.jp/~akhlaghi/reproduce/1505.01664/arxiv150501664.svg)](http://arxiv.org/abs/1505.01664)


## Introduction

The contents of this repository provide all the necessary files
(scripts and parameter files, managed through a Makefile) for the free
quantitative reproduction of the paper *Noise based detection and
segmentation of nebulous objects*, by Mohammad **Akhlaghi** and
Takashi **Ichikawa**, published in **The Astrophysical Journal
Supplement Series, 220, 1** (2015).


## Free quantitative reproduction

*Free quantitative reproduction* is defined to be the fact that all
the numbers and plots of the paper can be exactly reproduced through
the scripts in the `reproduce` directory. All the software used are
[free software](https://www.gnu.org/philosophy/free-sw.en.html)
enabling any curious scientist to easily check and experiment on the
source code of the software producing the results. Nature is already a
black box which we are trying hard to understand. Not letting other
scientists see the source code, or not allowing them to modify it (do
experiments on it) is a self-imposed black box, which only exacerbates
our ignorance.

Other scientists should be able to readily reproduce or check the
results of anything that is to carry the *scientific* label. Any
result that is not reproducible (due to incomplete information by the
author) is not scientific. Because the readers have to have faith in
the subjective experience of the authors in the very important choice
of configuration values and order of operations.



## Practical benefits

Reproducability is the main reason this system was designed. However,
a reproduction system like this also has lots of practical
benefits.

### While doing the research

For the actual researchers who are doing the research, this
methodology is very beneficial in the following aspects if it is
adhered to from the beginning:

* Other team members in a research project can easily
run/check/understand all the steps written by other members and find
possibly better ways of reaching the result or implement their part of
the research in a better fashion.

* During the research project, it might happen that one of the
parameters is decided to be changed or a new version of some of the
used software is released. With this system, updating all the numbers
and plots in the paper is as simple as running a `make` command and
the authors don't have to worry about part of the paper having the old
configuration and the other part with the new configuration. Manually
trying to change everything in the text will be prone to errors.

* If the referee asks for another set of parameters, they can be
immediately replaced and all the plots and numbers in the paper will
be correspondingly updated.

* Enabling version control on the contents of this reproduction
pipeline will make reverting back to a previous state extremely
simple, thereby enabling the researchers to experiment more with
alternative methods and new ideas, even in the middle of an on-going
research. For this first paper, this reproduce directory was only made
after the paper was accepted. Therefore version control is only
applied at the end (mainly to upload into Gitlab). But in our next
paper, we have applied it from the beginning and it extremely useful
as discussed above. Gitlab enables free private repositories which is
very useful for collaborations prior to publication.

* The authors are given the right to forget the details and keep their
mind open. In any situation they can simply refer back to these
scripts and see exactly what they did. This will enable researchers to
be more open to learning/trying new methods without worrying about
loosing/forgetting the details of their previous work. Since the
contents of the `reproduce` directory are inherently very small, they
can easily be uploaded on arXiv or other publication services and be
engraved along with the actual publication and thus be always
available, see [this paper's arxiv source
files](http://arxiv.org/format/1505.01664v2) for an example.


### After publication

* Other scientists can modify the parameters or the steps in order to
check the effect of those changes on the plots and reported numbers
and possibly find enhancements/problems in the result.

* It serves as an excellent repository for students, or scientists
with different specialties to master the art of data processing and
analysis in this particular sub-field. By removing this barrier, it
will enable the mixture of the experiences of the different fields,
potentially leading to new insights and thus discoveries.

* By changing the basic input parameters, the readers can try the
exact same steps on other data-sets and check the result on the same
paper that they have read and have become familiar with.

Of course, for all the situations above to be maximally effective, the
scripts have to be nicely/thoroughly commented.



## This implementation

The contents can be viewed online before downloading by clicking on
"Files" link on the left sidebar (also displayed with a couple of
files icon on the thin sidebar) or the value specifying the storage
space (in MB) above. The `reproduce` directory keeps all the scripts
and configuration files.

The basic idea is that after preparing the prerequisites (see
`reproduce/README.md` for more on the necessary FITS files and
parameters) the reader first goes into the `reproduce` directory and
runs the `make` command. Then they come back to the top directory and
run `make` again to make the paper's PDF, in short:

```
$ cd reproduce
$ make
$ cd ..
$ make
```


## Software requirements

To reproduce the results you will need the following programs
installed on the system. Other versions might give different results
so `reproduce/Makefile` checks the version of SExtractor and the
`--onlyversion=0.0` option is set in all [Gnuastro configuration
files](https://www.gnu.org/software/gnuastro/manual/html_node/Configuration-files.html). If
you have different versions installed, the scripts will not
complete. So if you want to use another version, manually change the
respective parameter(s). But beware that there might be unforeseen
problems since the scripts were made for these versions and their
outputs.

#### GNU Astronomy Utilities 0.0

[GNU Astronomy Utilities](https://www.gnu.org/software/gnuastro/)
(Gnuastro) 0.0 is a pre-release version labeled 0.0 since Gnuastro was
started off with this paper. It is available from the official
Gnuastro download link on its webpage. Gnuastro is still young and
under heavy development, so more recent versions might not produce
identical output.

#### Source Extractor 2.19.5

[Source Extractor](http://www.astromatic.net/software/sextractor)
2.19.5 is also used in this paper and has to be present on your
system.

#### AWK

AWK (the most common implementation is [GNU
AWK](https://www.gnu.org/software/gawk/)) with the executable name of
`awk`. AWK is already installed in most Unix-like operating systems
including GNU/Linux. AWK is extensively used in the scripts to manage
the numbers, catalogs and their reporting.

#### lscpu

`lscpu` only used close to the end to report the CPU frequency as
reported in the discussion section of the paper by
`scripts/papernumbers.sh`.

#### LaTeX

In order to compile the paper into a PDF, LaTeX is necessary, the best
place to get it is [TeX Live](https://www.tug.org/texlive/). All the
necessary packages are included in the standard install.


## Similar attempts

Fortunately other astronomers have also made similar attempts at exact
reproduction. A list can be seen below. Since there is no adopted
standard yet, each follows a different method which is not exactly
like this paper's reproduction pipeline. This is still a new concept
and thus such different approaches are great to make the concept more
robust. Please have a look at these methods too besides this paper's
style and adopt your own style and share it.

* Robitaille et
  al. ([2012](http://dx.doi.org/10.1051/0004-6361/201219073))
  Astronomy & Astrophysics, 545, A39. The reproduction scripts are
  available [on
  GitHub](https://github.com/hyperion-rt/paper-galaxy-rt-model).

* Parviainen et
  al. ([2016](http://dx.doi.org/10.1051/0004-6361/201526313))
  Astronomy & Astrophysics, 585, A114. The reproduction scripts are
  available on
  [GitHub](https://github.com/hpparvi/Parviainen-2015-TrES-3b-OSIRIS).

* Moravveji et
  al. ([2016](http://mnrasl.oxfordjournals.org/content/455/1/L67.abstract)). Monthly
  Notices of the Royal Astronomical Society, 455, L67. The
  reproduction information is available
  [on Bitbucket](https://bitbucket.org/ehsan_moravveji/op_mono/wiki/Home).


## Acknowledgments

Mohammad-reza Khellat and Alan Lefor kindly provided very useful
comments during the creation of this reproduction system. Ehsan
Moravveji, Mosè Giordano, and Paul Wilson kindly informed me of other
similar reproduction attempts.