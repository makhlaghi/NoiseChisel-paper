# Run LaTeX and BibLaTeX on a Tex source file. It is assumed that all
# the TeX source files are in a ./tex/ directory and the main .tex
# file is called paper.tex.
#
#
# TECHNICAL NOTE:
#
# If we simply use `$?' to check the return value, then it will be
# read from the make script and not within the shell script that is
# run within make. So we have to escape the $ sign with $$.

BIBname=./tex/ref.tex

paper.pdf: paper.tex tex/* paper.bbl reproduce/
#	rm -f ./tikz/paper-figure22.*
	mkdir -p tikz
	if [ -f paper.aux ]; then                                    \
	   if latex -shell-escape -halt-on-error paper.tex; then     \
	     echo "DVI made.";                                       \
	   else                                                      \
	     rm paper.aux;                                           \
	     exit 1;                                                 \
	   fi;                                                       \
	else                                                         \
	   if latex -shell-escape -halt-on-error paper.tex; then     \
	     echo "DVI made.";                                       \
	   else                                                      \
	     rm paper.aux;                                           \
	     exit 1;                                                 \
	   fi;                                                       \
	   latex -shell-escape paper.tex;                            \
	fi
	dvips -Ppdf paper.dvi
	ps2pdf paper.ps
#	rm paper.dvi
#	rm paper.ps
	if [ -f paper.blg ]; then rm paper.blg; fi
	if [ -f paper.run.xml ]; then rm paper.run.xml; fi
	if [ -f paper.auxlock ]; then rm paper.auxlock; fi
	rm paper.bcf paper.log paper.out

paper.bbl: $(BIBname)
	mkdir -p ./tikz/
	if latex -shell-escape -halt-on-error paper.tex; then        \
	  echo "DVI made.";                                          \
	else                                                         \
	  rm paper.aux;                                              \
	  exit 1;                                                    \
	fi;
	biber paper.bcf
	latex -shell-escape paper.tex;
